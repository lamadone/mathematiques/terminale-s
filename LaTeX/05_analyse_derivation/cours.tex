\chapter{Dérivation des fonctions}

Dans tout ce document, $f$ désigne une fonction continue et
$\intv{a}{b}$ un intervalle de $\R$.

\section{Nombre dérivé -- fonction dérivée}

\begin{definition}
	$f$ est dérivable en $x_0\in\intv[o]{a}{b}$ si
	$\lim_{\substack{h\to 0 \\ h<0}} \frac{f(x_0 + h) - f(x_0)}h
	= \lim_{\substack{h\to 0 \\ h>0}} \frac{f(x_0 + h) -
		f(x_0)}h = d$, un nombre fini.\\	
	Cette limite s'appelle \textbf{nombre dérivé} de $f$ en $x_0$ et se note
	$f'(x_0)$
\end{definition}%

\begin{center}
	\begin{tikzpicture}[scale=1,
	use Hobby shortcut,
	tangent/.style={%
		in angle={(180+#1)},
		Hobby finish,
		designated Hobby path=next,
		out angle=#1,
	},>=latex,
	]
	\draw [dotted, help lines, line width=0.6pt] (-6,-1) grid (6,6) ;
	\draw [dotted, help lines, step=0.5] (-6,-1) grid (6,6) ;
	\draw [very thick, ->] (0,-1) -- (0,6) ;
	\draw [very thick, ->] (-6,0) -- (6,0) ;
	\draw [thick] ([tangent=-45]-4,5) .. ([tangent=-45]-2.5,1.5) ..
	([tangent=0]-1,1) .. ([tangent=45]0,1.5) .. ([tangent=0]1,2) .. (2,1.5) ..
	([tangent=0]4,0) ;
	\draw[thick,->] (-2.5,1.5) -- +(-45:1) ;
	\draw[thick,->] (-2.5,1.5) -- +(-45:-1) ;
	\draw[thick,->] (0,1.5) -- +(45:1) ;
	\draw[thick,->] (0,1.5) -- +(45:-1) ;
	\foreach \x/\y in {-4/5,-2.5/1.5,0/1.5,2/1.5,4/0}
	{ \draw (\x,\y) node [circle,fill,inner sep=1pt] {} ; }
	\draw (-4,5) node [above right] {$\mathscr{C}_f$} ;
	\end{tikzpicture}
\end{center}

\begin{definition}
	La fonction dérivée de $f$ sur $\intv{a}{b}$ est la fonction
	qui à tout $x$ de $\intv{a}{b}$ associe le nombre dérivé en
	$x$.\\	
	On note cette fonction $f'$.
\end{definition}%

\begin{remarque}
	Dans certains cas, on ne peut associer de nombre dérivé en un point.\\
	Dans ce cas, la dérivée présente une discontinuité.
\end{remarque}

\begin{theoreme}
	\leavevmode
	\begin{itemize}
		\item Si $f$ est dérivable en $a$, alors $f$ est continue en
		$a$.
		\item Si $f$ est dérivable sur $I$ intervalle de $\R$, alors
		$f$ est continue sur $I$.
	\end{itemize}
\end{theoreme}
\begin{proof}
	Soit $f$ une fonction. Si $f$ est dérivable en $a$, alors $\lim_{h\to 0} \frac{f(a + h) - f(a)}h = f'(a)$ d'où \\
	$\lim_{h\to 0} \frac{f(a + h) - f(a) - hf'(a)}h = 0$. Par définition de la limite, $\frac{f(a + h) - f(a) - hf'(a)}h$ appartient à tout intervalle contenant 0 pour $h$ suffisamment proche de 0.\\
	Fixons $\varepsilon>0$. On a donc $-\varepsilon < \frac{f(a + h) - f(a) - hf'(a)}h < \varepsilon$, ce qui est équivalent à\\
	$-h\varepsilon < f(a + h) - f(a) - hf'(a) < h\varepsilon$, ou encore $h(f'(a)-\varepsilon) < f(a + h) - f(a) < h(f'(a)+\varepsilon)$.\\
	On a donc un encadrement, pour $h \to 0$ et par passage à la limite, on obtient que $\lim_{h\to 0} f(a + h) - f(a) = 0$ et donc que $f$ est continue en $a$.
	
	Le deuxième point est aisé à démontrer.
\end{proof}

\begin{remarque}
	La réciproque de ce théorème est fausse en général.
\end{remarque}

\begin{contreexemple}[Fonction continue en un point, mais non dérivable.]
	La fonction $x\mapsto \sqrt{x}$ est continue en 0, mais n'est pas dérivable en 0. 
\end{contreexemple}

\begin{info}[Fonction continue partout, dérivable nulle part]
	Georg Cantor, puis Hilbert ont proposé des fonctions qui sont continues partout, mais dérivable nulle part.
\end{info}

\begin{proposition}
	Soit $f$ et $g$ deux fonctions dérivables sur $I = \intv{a}{b} \subset \R$.
	\begin{itemize}
		\item $\forall x \in I,\ (f(x)+g(x))' = f'(x)+g'(x)$
		\item $\forall x \in I,\ (\lambda f(x))' = \lambda f'(x)$
	\end{itemize}
\end{proposition}
\begin{proof}
	Démonstration aisée, laissée en exercice au lecteur.
\end{proof}

On a aussi la propriété suivante sur la dérivée du produit de deux
fonctions dérivables.

\begin{proposition}\label{analyse:derivation:prop:produit}
	Soit $f$ et $g$ deux fonctions dérivables sur $I = \intv{a}{b} \subset \R$. Alors $\forall x \in I,\ (f(x)g(x))' = f'(x)g(x)+f(x)g'(x)$
\end{proposition}
\begin{proof}
	Soit $x_0 \in \intv[o]{a}{b}$.\\
	$f(x_0 + h)g(x_0 + h) - f(x_0)g(x_0) = f(x_0 + h)g(x_0 + h) - f(x_0)g(x_0 + h) + f(x_0)g(x_0 + h) - f(x_0)g(x_0)$.\\
	On a donc $\lim_{h\to 0} \frac{f(x_0 + h)g(x_0 + h) - f(x_0)g(x_0)}{h} =$\\
	$\lim_{h\to 0} \frac{f(x_0 + h)g(x_0 + h) - f(x_0)g(x_0 + h)}h + \lim_{h\to 0} \frac{f(x_0)g(x_0 + h) - f(x_0)g(x_0)}h$.\\
	$\lim_{h\to 0} \frac{f(x_0 + h)g(x_0 + h) - f(x_0)g(x_0 + h)}h = \lim_{h\to 0} g(x_0 + h)\frac{f(x_0 + h) - f(x_0)}h$. Or $g$ est continue, donc $\lim_{h\to 0} g(x_0 + h) = g(x_0)$ et le deuxième facteur n'est autre que $f'(x_0)$.\\
	De même, $\lim_{h\to 0} \frac{f(x_0)g(x_0 + h) - f(x_0)g(x_0)}h = f(x_0)\lim_{h\to 0} \frac{g(x_0 + h) - g(x_0)}h$, d'où le résultat annoncé.
\end{proof}

Cette propriété justifie que l'essentiel soit de construire le tableau
des dérivées.

\begin{center}
	\begin{tabular}{|p{5cm}|p{5cm}|}\hline
		$f:\R\to\R,\ x\mapsto k,\ k\in \R$ & $f':\R\to\R,\ x\to 0$ \\ \hline
		$f:\R\to\R,\ x\mapsto x$ & $f':\R\to\R,\ x\to 1$ \\ \hline
		$f:\R\to\R,\ x\mapsto x^2$ & $f':\R\to\R,\ x\to 2x$ \\ \hline
		$f:\R^*\to\R^*,\ x\mapsto \frac1x $ & $f':\R^*\to\R^*,\ x\to -\frac1{x^2}$ \\ \hline
		$f:\R_+\to\R_+,\ x\mapsto \sqrt{x}$ & $f':\R_+^* \to \R_+^*,\ x\to \frac1{2\sqrt{x}}$ \\ \hline
	\end{tabular}
\end{center}

\section{Dérivation d'une fonction composée}

\begin{proposition}
		Soit $u$ une fonction définie et dérivable sur $\intv{a}{b}$.\\
		$u^n$ est dérivable et sa dérivée est $x\mapsto nu'(x)u^{n-1}(x)$
\end{proposition}
\begin{proof}
	Se démontre par récurrence en utilisant la proposition~\ref{analyse:derivation:prop:produit} pour l'hérédité.
\end{proof}

\begin{exercise}
	Sans développer dériver la fonction définie pour tout réel par
	$f(x)=(x^2 + 2x -3)^3$.
\end{exercise}
\begin{solution}[print=true]
	$f'(x) = 3(2x + 2)(x^2 + 2x - 3)^2$.
\end{solution}

\begin{proposition}
	Soit $u$ une fonction définie et dérivable sur $\intv{a}{b}$. On suppose que $u(x)$ ne s'annule pas $\intv{a}{b}$.\\
	La fonction	$x\mapsto \frac1{u(x)}$ est dérivable et sa dérivée est
	$x\mapsto -\frac{u'(x)}{(u(x))^2}$.
\end{proposition}
\begin{proof}
	Soit $u$ une fonction définie et dérivable sur $\intv{a}{b}$. On suppose que $u(x)$ ne s'annule pas $\intv{a}{b}$.\\
	Posons $f = \frac1u$. On a, d'après la proposition~\ref*{analyse:derivation:prop:produit}, $\forall x \in \intv[o]{a}{b},\ (u(x)f(x))' = u'(x)f(x) + u(x)f'(x)$. Or $u(x)f(x) = 1$ donc $(u(x)f(x))' = 0$ et donc $u'(x)f(x) = -u(x)f'(x)$, ce qui entraîne que $f'(x) = -\frac{u'(x)}{u(x)}f(x)$. Et comme $f(x) = \frac1{u(x)}$, on obtient bien le résultat.
\end{proof}

\begin{exercise}
	Dériver $f\colon x\mapsto \frac1{x^2 +1}$
\end{exercise}
\begin{solution}[print=true]
	$f'(x) = \frac{-2x}{(x^2 + 1)^2}$
\end{solution}


\begin{proposition}
	Soit $u$ une fonction définie et dérivable sur $\intv{a}{b}$. On suppose que pour tout $x$ de $\intv{a}{b}$, $u(x) > 0$.\\
	$x\mapsto\sqrt{u(x)}$ est dérivable et sa dérivée est $x\mapsto
	\frac{u'(x)}{2\sqrt{u(x)}}$
\end{proposition}
\begin{proof}
	Soit $u$ une fonction définie et dérivable sur $\intv{a}{b}$. On suppose que pour tout $x$ de $\intv{a}{b}$, $u(x) > 0$.\\
	Posons $f = \sqrt{u}$. Ainsi $f^2 = u$ et donc $u'(x) = 2f'(x)f(x) = 2f'(x)\sqrt{u}$. On obtient ainsi $f'(x) = \frac{u'(x)}{2\sqrt{u(x)}}$.
\end{proof}


\begin{exercise}
	Dériver la fonction définie sur $\R$ par $f(x) = \sqrt{x^2 + 2x + 2}$
\end{exercise}
\begin{solution}[print=true]
	$f'(x) = \frac{2x + 2}{2\sqrt{x^2 + 2x + 2}}$
\end{solution}

\begin{theoreme}
		Sous réserve d'existence, la dérivée de $f(u(x))$, notée $f\circ
		u$ est $u'\times f'(u(x)) = u'\times (f'\circ u)$
\end{theoreme}
\begin{proof}
	Admis.
\end{proof}

