%:set ft=tex
\documentclass[a4paper,french,12pt]{article}

\input{commons.tex.inc}

\title{Corrections exercices intégration}
\date{Mars 2019}
\author{}

\everymath{\displaystyle{\everymath{}}}

\parindent0pt

\begin{document}

\begin{exercise}
  Les courbes d'équations $y= \sqrt{x}$ et $y = x^5$ déterminent, à
  l'intérieur du carré unité, trois domaines $\mathcal{D}_1$,
  $\mathcal{D}_2$, $\mathcal{D}_3$.

  Calculer, en unité d'aire, l'aire des trois domaines.
\end{exercise}
\begin{solution}[print=true]
  Il faut ici calculer les trois aires dans l'ordren en remarquant que
  $0 ≤ x ≤ 1 \implies 0 ≤ x^5 ≤ \sqrt{x} ≤ 1$.

  En particulier, $\mathcal{D}_1 = \int_0^1 x^5 \diff x =
  \brk[s]*{\frac16x^6}_0^1 = \frac16$.

  $\mathcal{D}_2 = \int_0^1 \sqrt{x} \diff x - \mathcal{D}_1$. Il faut donc
  trouver une primitive à $\sqrt{x}$. On peut remarquer que
  $\frac{\diff}{\diff x}(x \sqrt{x}) = \sqrt{x} + x \frac{1}{2\sqrt{x}} =
  \frac32 \sqrt{x}$. On en déduit que $∫_0^1 \sqrt{x} \diff x = \brk[s]*{
  \frac23 x \sqrt{x} }_0^1 = \frac23$ et donc que $\mathcal{D}_2 = \frac23
  - \frac16 = \frac12$

  L'aire $\mathcal{D}_3$ est l'aire restante et vaut donc $\frac13$.
\end{solution}


\begin{exercise}[subtitle={Intensité efficace en électricité}]
  L'intensité, à un instant $t ≥ 0$ d'un courant alternatif de période $T$
  s'exprime sous la forme : \[ i(t) = I_m \sin(\omega t) \] où $I_m$ est
  l'intensité maximale du courant et $\omega$ sa pulsation, avec $\omega =
  \frac{2\pi}{T}$.

  On appelle intensité efficace du courant, notée $I_e$ l'intensité d'un
  courant continu qui produit en une période le même effet calorifique à
  travers un conducteur ohmique de résistance $R$.

  Ainsi, l'énergie $W$ fournie dans l'intervalle $\intv{0}{T}$ est $W = R
  \int_0^T i^2(t) \diff t$ en courant alternatif et $W = RI_e^2T$ en courant
  continu.
  \begin{enumerate}
    \item  Prouver que $I_e^2$ est la valeur moyenne de la fonction $i^2$ sur
      $\intv{0}{T}$.
    \item Exprimer $I_e$ en fonction de $I_m$.
  \end{enumerate}
\end{exercise}
\begin{solution}[print=true]
  Comme souvent, malgré la situation issue de la physique, la résolution de
  ce problème ne mobilise pas de connaissances particulière en physique, ni
  en électricité.
  \begin{enumerate}
    \item La définition de $I_e^2$ permet d'égaler les deux expressions
      données de $W$et on a donc $RI_e^2T = R ∫_0^T i^2(t) \diff t \implies
      I_e^2T = ∫_0^T i^2(t) \diff t \implies I_e^2 = \frac1T ∫_0^T i^2(t)
      \diff t$. $\qed$
    \item Calculons $∫_0^T i^2(t) \diff t$.

      On a $∫_0^T i^2(t) \diff t = I_m^2 \int_0^T \sin^2(\omega t) \diff t =
      I_m^2 \int_0^T \frac{1 - \cos (2\omega t )}{2} \diff t.$

      On peut séparer en deux parties cette intégrale (linéarité) :
      \begin{itemize}
        \item $I_m^2 ∫_0^T \frac{1}2 \diff t = I_m^2 \frac{T}2$
        \item $I_m^2 ∫_0^T \frac{\cos (2\omega t )}{2} \diff t = 0$. En effet, ce
          dernier résultat peut s'observer graphiquement. (on peut aussi
          faire le calcul)
      \end{itemize}
      On en tire que $I_e^2 = \frac{I_m^2}{2}$ et donc $I_e =
      \frac{I_m}{\sqrt{2}} = \frac{\sqrt{2}}2 I_m \approx \np{0.707} × I_m$.
  \end{enumerate}
\end{solution}

\begin{exercise}[subtitle={De la conjecture à la preuve}]
  $(I_n)$ désigne la suite définie pour tout $n$ de $\N^*$ par : \[ (I_n) =
  \int_0^1 x^n e^{-x} \diff x . \]
  \begin{enumerate}
    \item Calculer $I_1$
    \item Pour tout entier naturel non nul, on note $\mathscr{C}_n$ la
      représentation graphique de la fonction $f_n$ définie sur
      $\intv{0}{1}$ par : \[ f_n(x) = x^ne^{-x} . \]
      Ainsi, pour tout naturel $n$ non nul : \[ I_n = \int_0^1 f_n(x) \diff
        x . \] On a tracé ci dessous $\mathscr{C}_1, \mathscr{C}_2,
        \mathscr{C}_3, \mathscr{C}_{10}, \mathscr{C}_{20},
        \mathscr{C}_{30}$.
        \begin{enumerate}
          \item Formulez une conjecture sur le sens de variation de la suite
            $(I_n)$ en décrivant votre démarche.
          \item Démontrez cette conjecture.
          \item Déduisez en que la suite $(I_n)$ converge.
          \item Déterminez $\lim_{n\to \infty} I_n$.
        \end{enumerate}
  \end{enumerate}
\end{exercise}
\begin{solution}[print=true]
  \begin{enumerate}
    \item $I_1 = \int_0^1 x e^{-x} \diff x$. En remarquant que
      $\frac{\diff}{\diff x}(xe^{-x}) = e^{-x} - xe^{-x}$, on trouve que
      $xe^{-x} = e^{-x} - \frac{\diff}{\diff x}(xe^{-x})$. Ainsi, une
      primitive de $xe^{-x}$ est $-e^{-x} - xe^{-x}$ et donc $I_1 = 1 -
      2e^{-1}$.
    \item
      \begin{enumerate}
        \item La suite semble être décroissante.
        \item Soient $n,m \in (\N^*)^2,\ n ≤ m$ et $x \in \intv{0}{1}$. On a
          $x^n ≥ x^m$ et donc $f_n(x) ≥ f_m(x)$. L'intégrale conservant les
          inégalités, on a donc $n ≤ m \implies I_n ≥ I_m$. La suite est
          donc décroissante.
        \item La suite $(I_n)$ est strictement décroissante et il s'agit
          d'une suite d'intégrale de fonctions positives, donc la suite est
          minorée par 0. Elle est donc convergente.
        \item Pour tout $n \in \N^*$ et pour tout $x \in \intv{0}{1},\ x^n ≥
          x^n e^{-x} ≥ 0$. On en déduit que $0 ≤ I_n ≤ \int_0^1 x^n \diff x
          = \frac1n$. Le théorème d'encadrement permet de conclure.

          Si on veut une relation de récurrence :

          Soit $n \in \N^*, n ≥ 2$. On a $f'_n(x) = nx^{n-1} e^{-x} -
          x^n e^{-x}$. On en déduit que $\int_0^1 f'_n(x) \diff x = nI_{n-1}
          - I_n$, ce qui s'écrit aussi $e^{-1} = nI_{n-1} - I_n \iff I_n =
          nI_{n-1} - e^{-1}$. En changeant les indices, on obtient, pour
          tout $n \in \N^*,\ I_{n+1} = (n+1)I_n - e^{-1}$.
      \end{enumerate}
  \end{enumerate}
\end{solution}

\end{document}

