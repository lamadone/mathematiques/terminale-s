\documentclass[french]{beamer}

\input{prez.tex.inc}

\usetheme{Madrid}

\title{Suite et récurrence}
\subtitle{Généralités et raisonnement}
\author{Vincent-Xavier \bsc{Jumel}}
\institute{LaSalle Saint-Denis}
\date{5 septembre 2018}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

\section{Généralités}
	
\subsection{Définition}

\begin{frame}
	\frametitle{Définitions}
	\begin{definition}
		Une suite numérique est une «collection infinie» de nombres numérotées à 
		partir de 0.\\
		On représente une telle suite par une lettre, définissant la suite dans 
		son ensemble et on note $u$ ou $(u_n)_{n\in\N}$.\\
		$u_n$ s'appelle le \emph{terme général} de la suite $u$.
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Quelques exemples}
	\begin{exemple}
		\begin{itemize}
			\item La suite des nombre entiers ;
			\item la suite des entiers pairs ;
			\item la suite des nombres premiers ;
			\item la suite de terme général $p_n = \pi^n$ ;
			\item …
		\end{itemize}
		sont des suites numériques.
	\end{exemple}
\end{frame}

\begin{frame}
	\frametitle{Attention}
	\begin{block}{}
		Attention : bien noter $u_n$ et non $u$\scriptsize$n$\normalsize.
	\end{block}
	
	\begin{remarque}
		Une «collection finie» s'appelle une \emph{séquence} ou une 
		\emph{famille}.
	\end{remarque}
\end{frame}

\begin{frame}
	\frametitle{Suite constante}
	\begin{definition}\label{analyse:suites:def:constante}
		Soit $a$ un nombre réel. On appelle suite constante de terme général $a$ 
		la suite dont tous les termes valent $a$.\\
		Si $a = 0$ , on parle alors de suite nulle.\\
		Par abus de notation, on confond les réels $a$ et les suites constantes 
		de terme général $a$.
	\end{definition}
\end{frame}

\begin{frame}
\frametitle{Arithmétique et géométrique}
	\begin{definition}
		\leavevmode
		\begin{itemize}
			\item On appelle \emph{suite arithmétique} de premier terme $u_0$ et de 
			raison $r$ toute suite définie par $u_0$ et $\forall n \in \N,\ u_{n+1} 
			= u_n + r$.
			\item On appelle \emph{suite géométrique} de premier terme $v_0$ et de
			raison $q$ toute suite définie par $v_0$ et $\forall n \in  \N,\ 
			v_{n+1} = v_n × q$
		\end{itemize}
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Opérations}
	\begin{definition}
		Soient $u$ et $v$ deux suites de terme général $u_n$ et $v_n$.
		\begin{itemize}
			\item La suite $u+v$ est la suite de terme général $u_n + v_n$
			\item La suite $uv$ est la suite de terme général $u_n × v_n$.
		\end{itemize}
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Variation d'une suite}
	\begin{definition}
		On dit qu'une suite est croissante, respectivement décroissante, lorsque 
		pour tout $n$ entier naturel, $u_n ≤ u_{n+1}$, respectivement $u_{n+1} ≤ 
		u_n$.\\
		Lorsque les inégalités sont strictes ($<$ ou $>$), on dit strictement 
		croissante ou strictement décroissante.\\
		Si les premiers termes de la suite ne sont pas ordonnées, on dit que la 
		suite est croissante, respectivement décroissante, à partir d'un certain 
		rang. On a alors, pour tout $n$ entier naturel, il existe un rang $n_0$, 
		tel que si $n ≥ n_0$, alors $u_n ≤ u_{n+1}$.\\
		Une suite croissante ou décroissante est dite \textbf{monotone}.
	\end{definition}
	\begin{remarque}
		Cette définition est tout à fait compatible avec la 
		définition~\ref{analyse:suites:def:constante}.
	\end{remarque}
\end{frame}

\begin{frame}
	\frametitle{Déterminer si une suite est croissante}
	\begin{proposition}
		\leavevmode
		\begin{itemize}
			\item $u$ est croissante si et seulement si, pour tout $n$ entier 
			naturel, $u_{n+1} - u_n ≥ 0$.
			\item Si $u$ est à valeurs strictement positives, $u_n$ est croissante 
			si et seulement si, pour tout $n$ entier naturel, $\frac{u_{n+1}}{u_n} 
			> 1$.
			\item Si $u$ et $v$ sont deux suites croissantes, alors $u+v$ est 
			croissante et, si de plus, $u$ et $v$ sont à valeurs positives, $uv$ 
			est croissante.
		\end{itemize}
	\end{proposition}
\end{frame}
\begin{frame}
	\begin{proof}
		\leavevmode
		\begin{itemize}
			\item Laissée en exercice.
			\item Laissée en exercice.
			\item Si $u$ est croissante, alors $u_{n+1} - u_n ≥ 0$. De même, si $v$ 
			est croissante, $v_{n+1} - v_n ≥ 0$. Il vient donc que $u_{n+1} - u_n + 
			v_{n+1} - v_n ≥ 0$ et donc $(u_{n+1}+v_{n+1})-(u_n+v_n) ≥ 0$.\\
			Soient $u$ et $v$ deux suites croissantes, à valeurs strictement 
			positives. On a alors $\frac{u_{n+1}}{u_n} ≥ 1$ et $\frac{v_{n+1}}{v_n} 
			≥ 1$ d'où $\frac{u_{n+1}v_{n+1}}{u_nv_n} ≥ 1$.
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\frametitle{Vocabulaire}
	\begin{definition}
		Soit $E$ un ensemble de nombres, par exemple $E = \setcond{u_n}{n \in \N}$
		\begin{itemize}
			\item On dit que $m$ est un \textbf{minorant} de $E$ (ou de la suite), 
			lorsque, pour tout $x \in E,\ m ≤ x$. On dit alors que $E$ (ou la 
			suite) est \textbf{minoré}.
			\item On dit que $M$ est un \textbf{majorant} de $E$ (ou de la suite), 
			lorsque, pour tout $x \in E,\ x ≤ M$. On dit alors que $E$ (ou la 
			suite) est \textbf{majoré}.
			\item Un ensemble qui est à la fois majoré et minoré est dit 
			\textbf{borné}.
		\end{itemize}
	\end{definition}
	
	\begin{remarque} On peut parfois parler de minoration (ou majoration) par 
		une suite termes à termes, lorsque, pour tout entier naturel $n$, $u_n ≤ 
		v_n$. On note alors, abusivement $u ≤ v$.
	\end{remarque}
\end{frame}

\subsection{Raisonnement par récurrence}

\begin{frame}
	\frametitle{Raisonnement par récurrence}
	\begin{block}{}
	Le raisonnement par récurrence est un mode de raisonnement particulier aux 
	entiers naturels, ou aux éléments d'une suite indexée par les entiers 
	naturels. Il permet en particulier de montrer une propriété sur tous les 
	éléments d'une suite, en vérifiant la propriété sur le premier élément de 
	la suite, puis en montrant que supposer la propriété vraie pour un élément 
	quelconque entraîne que la proposition est vraie pour l'élément suivant.
	\end{block}
	\begin{theoreme}
		Soit $\P_n$ une proposition indexée par $n$. Si $\P_0$ est vraie et si 
		$\P_n \implies \P_{n+1}$, alors $\forall n \in \N,\ \P_n$ est vraie.
	\end{theoreme}

	\begin{info}[Histoire]
		Ce théorème est un axiome de la théorie des entiers naturels proposée par 
		Guieseppe Peano.
	\end{info}
\end{frame}

\begin{frame}[allowframebreaks]{}
\begin{exemple}
	Démontrons que la somme des entiers naturels de 0 à $n$ s'écrit 
	$\sum_{k=0}^n k = \frac{n(n+1)}{2}$.\\
	Soit $\P_n$ la proposition «$\sum_{k=0}^n k = \frac{n(n+1)}{2}$».\\
	La somme s'initialisant à 0, on a, pour $n = 0$, $\sum_{k=0}^0 = 0$ et 
	$\frac{0×(0+1)}{2} = 0$. La proposition $\P_0$ est donc vraie.
	\end{exemple}
	\begin{exemple}
	Soit $n$ un entier arbitrairement choisi. Supposons $\P_n$ vraie et
	démontrons $\P_{n+1}$. $\P_n$ vraie s'écrit $\sum_{k=0}^n k = 
	\frac{n(n+1)}{2}$.
	\begin{align*}
	\sum_{k=0}^{n+1} k & = \frac{n(n+1)}{2} + (n+1) \\
	& = \frac{n(n+1)}{2} + \frac{2(n+1)}{2} \\
	& = \frac{n(n+1) + 2(n+1)}{2} \\
	& = \frac{(n+2)(n+1)}{2}
	\end{align*}
	On peut conclure que, pour tout $n$ entier naturel, $\sum_{k=0}^n k = 
	\frac{n(n+1)}{2}$.
\end{exemple}
\end{frame}

\begin{frame}{Rédaction}
	\begin{savoirrediger}[Rédiger une démonstration par récurrence]
		Pour rédiger une démonstration par récurrence, il faut :
		\begin{itemize}
			\item bien identifier la proposition $\P_n$ et l'écrire explicitement;
			\item écrire l'étape d'\textbf{initialisation}, c'est-à-dire la 
			vérification de la proposition au rang initial ;
			\item écrire l'étape d'\textbf{hérédité}, en mentionnant l'usage de la 
			proposition $\P_n$, appelée ici \textbf{hypothèse de récurrence} ;
			\item écrire la \textbf{conclusion}.
		\end{itemize}
	\end{savoirrediger}
\end{frame}

%\begin{frame}
%
%\begin{multicols}{2}
%
%\begin{exercise}
%	Démontrer par récurrence que, pour tout entier $n ≥ 3$, $2^n  > 2n$.
%\end{exercise}
%
%\begin{exercise}
%	$u$ est la suite définie par $u_0 = 0$ et pour tout $n$ entier naturel, 
%	\[u_{n+1} = \sqrt{u_n+5}.\]
%	\begin{enumerate}
%		\item Calculer explicitement les premiers termes.
%		\item Démontrer par récurrence que, pour tout entier naturel $n ≥ 1$, 
%		$0 < u_n < 3$
%		\item Démontrer par récurrence, que pour tout entier naturel $n$, 
%		$u_n 
%		< u_{n+1}$.
%	\end{enumerate}
%\end{exercise}
%
%\begin{exercise}
%	$u$ est la suite définie par $u_0 = 2$ et pour tout nombre entier 
%	naturel
%	               $n$, \[ u_{n+1} = \sqrt{7u_n}.\]
%	\begin{enumerate}
%		\item Démontrer par récurrence que pour tout nombre entier naturel 
%		$n$, 
%		$0 ≤ u_n ≤ 7$.
%		\item Démontrer par récurrence que la suite $u_n$ est décroissante.
%	\end{enumerate}
%\end{exercise}
%
%\begin{exercise}
%$u$ est la suite définie par $u_0 = 10$ et pour tout nombre entier 
%naturel 
%$n$, \[ u_{n+1} = \frac12u_n+1.\]
%\begin{enumerate}
%	\item Démontrer par récurrence que pour tout nombre entier naturel 
%	$n$, 
%	$u_n ≥ 0$.
%	\item Démontrer par récurrence que la suite $u_n$ est décroissante.
%\end{enumerate}
%\end{exercise}
%
%\begin{exercise}
%Démontrer par récurrence que pour tout entier $n,\ 2^n ≥ n + 1$.
%\end{exercise}
%
%\begin{exercise}
%Démontrer par récurrence que pour tout nombre entier $n ≥ 2, 4^n ≥ 4n 
%+1$.
%\end{exercise}
%
%\end{multicols}
%
%\end{frame}

\subsection{Quelques méthodes sur les suites}

\begin{frame}{Un algorithme}
	\begin{savoirfaire}[Écrire un algorithme de calcul des termes d'une suite 
		définie par une relation de récurrence.]
		\begin{align*}
		& U ← \text{valeur initiale}\\
		& N ← 0\\
		& \text{Tant que } N < \text{seuil}\\
		& \hspace{5mm} U ← \text{expression contenant } U\\
		& \hspace{5mm} N ← N + 1\\      
		\end{align*}
	\end{savoirfaire}
\end{frame}

\begin{frame}{Représentation graphique}

\begin{savoirfaire}[Tracer les points d'une suite définie par une relation 
de récurrence.]
Soit $u$ définie par $u_0$ et pour tout $n$ entier naturel, $u_{n+1} = 
f(u_n)$.
\begin{itemize}
	\item Tracer la courbe représentative de la fonction $f$ dans un repère.
	\item Tracer, dans le même repère, la droite d'équation $y = x$.
	\item Placer $u_0$ sur l'axe des abscisses.
	\item Reporter cet abscisse sur la courbe de la fonction $f$.
\end{itemize}
\end{savoirfaire}
\end{frame}
\begin{frame}
\begin{center}
	\begin{tikzpicture}[scale=2]
	\draw [very thin,dashed,gray] (0,0) grid[step=0.5] (5,3.2) ;
	\draw [thin,dashed] (0,0) grid[] (5,3.2) ;
	\draw [thick, ->] (0,0) -- (5.1,0) ;
	\draw [thick, ->] (0,0) -- (0,3.2) ;
	\draw [very thick, ->] (0,0) -- (1,0) ;
	\draw (0.5,0) node [below] {$\vv{\imath}$} ;
	\draw [very thick, ->] (0,0) -- (0,1) ;
	\draw (0,0.5) node [left] {$\vv{\jmath}$} ;
	\draw (0,0) node [below left] {$O$} ;
	\draw [thick,blue] plot [domain=0:5,smooth] (\x, {sqrt(\x+5)} ) ;
	\draw [thick,red] (0,0) -- (3.2,3.2) ;
	
	\draw [blue] (0,0) node [draw,circle,inner sep=1] {} ;
	\draw [blue] (0,0) -- (0,2.236) node [draw,circle,inner sep=1] {} -- 
	(2.236,2.236) 
	node [draw,circle,inner sep=1pt] {};
	\draw [blue] (2.236,2.236) -- (2.236,2.690) node [draw,circle,inner 
	sep=1] {} -- (2.690,2.690) node [draw,circle,inner sep=1] {};
	\draw [blue] (2.690,2.690) -- (2.690,2.773) node [draw,circle,inner 
	sep=1] {};
	\draw [blue] (2.236,2.236) -- (2.236,0) node [draw,circle,inner 
	sep=1pt] {} ; \draw (2.236,0) node [below] {$u_1$ } ;
	\draw [blue] (2.690,2.690) -- (2.690,0) node [draw,circle,inner 
	sep=1pt] {} ; \draw (2.690,0) node [below] {$u_2$ } ;
	\draw [blue] (2.773,2.773) -- (2.773,0) node [draw,circle,inner 
	sep=1pt] {} ; \draw (2.773,0) node [below right] {$u_3$ } ;
	\end{tikzpicture}
\end{center}


\end{frame}
\end{document}
