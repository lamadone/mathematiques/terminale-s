%
%Donnons tout d'abord une présentation générale de la notion
%d'intégration avant de démontrer, dans le formalisme du langage
%mathématiques, un certain nombre de résultat.
%
%Intégrer une fonction, ou en effectuer la quadrature, c'est initialement
%rechercher un rectangle de même aire. Le problème le plus célèbre est
%celui de la quadrature du cercle dont Gauss puis Wantzel démontrèrent
%qu'il n'avait pas de solution\footnote{à la règle et au compas}. Cette
%recherche extrèmement féconde amena les mathématiciens de diverses
%époques à élaborer une nouvelle théorie, celle de l'intégration. Cette
%théorie constitue désormais un fondement des mathématiques, en
%particulier lorsque Lebesgue puis Kolmogorov l'associèrent aux
%probabilités.
%
%Afin de rendre ce problème dans le langage moderne, nous étudierons
%d'abord la vision géométrique dans le cas des fonctions continues
%positives puis nous verrons comment étendre cette définition aux
%fonctions continues et ensuite, nous verrons comment calculer
%effectivement une intégrale dans des cas simples. Pour conclure, nous
%ferons le lien de cette intégrale avec d'autres domaines des
%mathématiques ou des sciences.
%
%\bigskip
%
%Dans ce cours, $f$ désigne une fonction définie et continue sur $I =
%\interff{a;b}$ un intervalle de $\R$ où $a<b$ sont deux nombres réels.
%On notera $\Cf_f$ sa courbe représentative de la fonction $f$.

\chapter{Intégration sur un segment}

\section{Calculer une aire}

\begin{minipage}{0.74\linewidth}
On va dans un premier temps s'intéresser aux fonctions positives. Pour
$f$ une telle fonction, notons $\mathscr{D}(f)(a,b)$ le domaine limité
par $\Cf_f$, l'axe des abscisses $y=0$ et les droites d'équations $x=a$
et $x=b$.
\end{minipage}
\begin{minipage}{0.24\linewidth}
\begin{tikzpicture}
  \tkzInit[xmin=0,ymin=0,xmax=4.5,ymax=3.5]
  \tkzDrawXY[noticks=true]

  \draw [fill=lightgray!50] plot [smooth, domain=1:4] (\x,{(\x -1)*(\x
  -3)*(\x -4) + 1}) -- (4,0) -- (1,0) -- cycle ;
  \draw [thick] plot [smooth, domain=1:4] (\x,{(\x -1)*(\x -3)*(\x -4) +
  1}) ;

  \draw [thick] (1,0) node [below] {$a$} -- (1,1) node [fill, circle,
  inner sep = 1pt] {} ;
  \draw [thick] (4,0) node [below] {$b$} -- (4,1) node [fill, circle,
  inner sep = 1pt] {} ;
\end{tikzpicture}
\end{minipage}

\subsection{Définition}

\begin{minipage}{0.74\linewidth}
\begin{definition}[Sommes de Darboux]
  On considère une subdivision de l'intervalle $\intv{a}{b}$ en $n$
  points équidistants et on considère les rectangles construits en $f(a
  + i\Delta x)$ pour $i$ variant de 0 à $n -1$.

  La somme $\sum_{i=0}^nf(a + i\Delta x)\Delta x$ s'appelle somme de
  Darboux de $f$ sur l'intervalle $\intv{a}{b}$.
\end{definition}
\end{minipage}
\begin{minipage}{0.24\linewidth}
\begin{tikzpicture}
  \tkzInit[xmin=0,ymin=0,xmax=4.5,ymax=3.5]
  \tkzDrawXY[noticks=true]

  \draw [thick] plot [smooth, domain=1:4] (\x,{(\x -1)*(\x -3)*(\x -4) +
  1}) ;

  \draw [thick] (1,0) node [below] {$a$} -- (1,1) node [fill, circle,
  inner sep = 1pt] {} ;
  \draw [thick] (4,0) node [below] {$b$} -- (4,1) node [fill, circle,
  inner sep = 1pt] {} ;

  \foreach \x in {0,1,...,9} {
    \draw ({1+\x*3/10}, 0) rectangle ({1+(\x+1)*3/10},
    {(1+(\x+1)*3/10-1)*(1+(\x+1)*3/10 -3)*(1+(\x+1)*3/10 -4) +1}) ;
  }
\end{tikzpicture}
\end{minipage}

On peut observer plusieurs choses :
\begin{itemize}
  \item La somme des rectangles approche le domaine ;
  \item en ne prenant que des rectangles sous la courbe, la somme sera
    plus incluse dans le domaine ;
  \item en ne prenant que des rectangles au dessus de la courbe, le
    domaine sera inclus dans la somme.
\end{itemize}

\begin{definition}[Intégrale d'une fonction positive]\label{def:int_pos}
  On appelle intégrale d'une fonction positive l'aire du domaine limité
  par $\Cf_f$, l'axe des abscisses $y=0$ et les droites d'équations
  $x=a$ et $x=b$.

  On le note $\int_a^bf(x)\diff{x}$.
\end{definition}

\begin{remarque}
  \begin{itemize}
    \item L'écriture précédente se lit «intégrale de $a$ à $b$ de $f$ de
      $x$, $\diff{x}$».
    \item On dit aussi parfois «somme intégrale».
    \item Le $\int$ est un \textsl{S} allongé.
    \item Le $\diff{x}$ indique la variable d'intégration : ainsi
      $\int_a^bf(x)\diff{x} = \int_a^bf(t)\diff{t}$.
    \item Physiquement, cet élément différentiel est homogène à une
      longueur ou à un temps.
  \end{itemize}
\end{remarque}

\begin{exercise}
  À l'aide d'un raisonnement géométrique, donner l'intégrale entre $0$
  et $1$ des fonctions suivantes :
  \begin{itemize}
    \item la fonction constante égale à 1.
    \item la fonction constante égale à $k$, un réel strictement
      positif.
    \item la fonction $x\mapsto x$.
    \item la fonction $x\mapsto 2x+3$.
  \end{itemize}
  \emph{Indication :} Dessinez les fonctions dans un repère et reprenez
  les formules de calcul d'aires.
\end{exercise}

\begin{exercise}
  Toujours à l'aide d'un raisonnement géométrique, expliquer comment on
  peut décomposer l'aire de la dernière des fonctions.
\end{exercise}

\begin{exercise}
  Donner, toujours par un raisonnement géométrique, l'intégrale entre
  $0$ et $t$, $t$ un réel strictement positif de la fonction $x\mapsto
  x$.
\end{exercise}

Une activité permettra de démontrer que $\int_0^1x^2\diff{x} = \frac13$.

\subsection{Propriétés qui découlent de l'aire}

Du fait de la définition en tant qu'aire, on a les propriétés suivantes
qui en découlent.

\begin{proposition}[Positivité]
  L'intégrale d'une fonction positive est positive.
\end{proposition}
\begin{proof}
  C'est une aire.
\end{proof}

\begin{proposition}[Relation de Chasles]
  Pour tout $c \in \intv{a}{b}$, on $\int_a^bf(t)\diff{t} =
  \int_a^cf(t)\diff{t} + \int_c^bf(t)\diff{t}$.
\end{proposition}
\begin{proof}
  L'aire est une grandeur extensive.
\end{proof}

\begin{proposition}[Additivité de l'intégrale d'une fonction positive]
  \label{prop:linearite}
	\leavevmode
  \begin{itemize}
    \item Si $f$ et $g$ sont deux fonctions continues sur $I$, alors
      $\int_a^b f(x)+g(x) \diff{x} = \int_a^b f(x) \diff{x} +
      \int_a^b g(x) \diff{x}$.
    \item Si $\lambda$ est un réel, alors $\int_a^b\lambda f(x)\diff{x}
      = \lambda\int_a^b f(x)\diff{x}$
  \end{itemize}
\end{proposition}
\begin{proof}
  La preuve repose ici sur une notion intuitive et non développée
  rigoureusement. Supposons $f$ et $g$ strictement croissante sur tout
  l'intervalle $I$.
  \begin{itemize}
    \item À l'aide de somme de Darboux, je peux minorer l'intégrale de
      $f+g$. En écrivant une somme de Darboux démarrant de $b$, je peux
      également majorer l'intégrale de $f + g$.

      \begin{tikzpicture}
        \begin{scope}[scale=0.7]
          \tkzInit[xmin=0,ymin=0,xmax=4.5,ymax=4.5]
          \tkzDrawXY[noticks=true]

          \draw [thick] plot [smooth, domain=1:4] (\x,{(2*ln(\x) +
          1)/2}) node[right] {$f$};

          \draw [thick] (1,0) node [below] {$a$} -- (1,{(2*ln(1)+1)/2})
          node [fill, circle, inner sep = 1pt] {} ;
          \draw [thick] (4,0) node [below] {$b$} -- (4,{(2*ln(4) +
          1)/2}) node [fill, circle, inner sep = 1pt] {} ;

        \end{scope}
        \begin{scope}[scale=0.7,xshift=7cm]
          \tkzInit[xmin=0,ymin=0,xmax=4.5,ymax=4.5]
          \tkzDrawXY[noticks=true]

          \draw [thick] plot [smooth, domain=1:4] (\x,{exp(\x/4) }) node
          [right] {$g$};

          \draw [thick] (1,0) node [below] {$a$} -- (1,{exp(1/4)}) node
          [fill, circle, inner sep = 1pt] {} ;
          \draw [thick] (4,0) node [below] {$b$} -- (4,{exp(4/4)}) node
          [fill, circle, inner sep = 1pt] {} ;

        \end{scope}
        \begin{scope}[scale=0.7,xshift=14cm]
          \tkzInit[xmin=0,ymin=0,xmax=4.5,ymax=4.5]
          \tkzDrawXY[noticks=true]

          \draw [thick] plot [smooth, domain=1:4] (\x,{(2*ln(\x) + 1)/2}) ;

          \draw [thick] plot [smooth, domain=1:4] (\x,{(2*ln(\x) + 1)/2
          + exp(\x/4)}) node [right] {$f+g$};

          \draw [thick] (1,0) node [below] {$a$} --
          (1,{(2*ln(1)+1)/2+exp(1/4)}) node [fill, circle, inner sep =
          1pt] {} ;
          \draw [thick] (4,0) node [below] {$b$} --
          (4,{(2*ln(4)+1)/2+exp(4/4)}) node [fill, circle, inner sep =
          1pt] {} ;

        \end{scope}
      \end{tikzpicture}

      On a alors, par extensivité de la somme (cette fois verticalement)
      le résultat annoncé.
    \item On reprend le raisonnement précédent, mais cette fois avec
      $f$ et $\lambda f$.
  \end{itemize}
\end{proof}

\begin{remarque}
  On démontrera plus tard que ce résultat est vrai pour une fonction
  quelconque.
\end{remarque}

\begin{proposition}[Inégalité de la moyenne]
  Pour toute fonction $f$ continue positive, il existe $m$ et $M$ tels
  que \[ m(b-a) \leqslant \int_a^b f(x)\diff{x} \leqslant M(b-a)\]
\end{proposition}
\begin{proof}
  $f$ est continue donc d'après le théorème des valeurs intermédiaires,
  l'image de l'intervalle $\intv{a}{b}$ est un intervalle. La
  positivité de $f$ entraîne que cet intervalle est inclus dans les
  réels positifs, on a donc l'existence de $m$ et $M$, tels que $\forall
  x \in \intv{a}{b}\, 0 \leqslant m \leqslant f(x) \leqslant M$.

  Donc l'aire sous la courbe -- c'est-à-dire l'intégrale -- est comprise
  entre les aires de deux rectangles de longueur $b - a$ et de hauteurs
  respectives $m$ et $M$.
\end{proof}

\begin{definition}[Valeur moyenne]
  On appelle valeur moyenne de l'intégrale sur $\intv{a}{b}$ le réel
  noté $\mu_{\intv{a}{b}} = \frac1{b - a}\int_a^b f(x)\diff{x}$.
\end{definition}

\begin{remarque}
  Ce réel existe, il appartient même à l'intervalle image de $f$.
\end{remarque}

\subsection{Continuité}

Après le dernier exercice et l'activité, il est tentant de considérer
une nouvelle fonction $\Phi \colon \intv{a}{b} \to \R, x\mapsto
\int_a^xf(t)\diff{t}$.

\begin{definition}
  On pose -- c'est presque axiomatique -- $\int_a^af(t)\diff{t} = 0$.
\end{definition}

Avec cette définition, la fonction $\Phi$ est bien définie sur
$\intv{a}{b}$.

\begin{proposition}
  La fonction $\Phi$ est continue sur $\intv{a}{b}$.
\end{proposition}

En fait, on va même démontrer mieux que ça : $\Phi$ est dérivable et sa
dérivée est $f$.

\begin{proof}
  \leavevmode
  \\
  \begin{minipage}{0.65\linewidth}
    On va supposer ici que $f$ est croissante.  Soit $x_0$ un réel de
    l'intervalle $\intv{a}{b}$. Soit $h$ positif. Comparer $f(x_0)$ et
    $f(x_0 + h)$.

    Comme $h$ est positif et $f$ croissante, on $f(x_0) ≤ f(x_0 + h)$ et
    donc $h f(x_0) ≤ h f(x_0 + h)$. Par extensivité de l'intégrale,
    $\Phi(x_0 +h) - \Phi(x_0)$ est précisément l'aire sous la courbe et
    celle-ci est précisément encadrée par $h f(x_0)$ et $h f(x_0 + h)$. On
    peut donc en déduire que $f(x_0) \leqslant \frac{\Phi(x_0 + h) -
    \Phi(x_0)}{h} \leqslant f(x_0 + h)$.

    On obtient une inégalité similaire pour $h < 0$.
  \end{minipage}
  \begin{minipage}{0.25\linewidth}
    \begin{tikzpicture}[xscale=1.4]
      \tkzInit[xmin=0,ymin=0.5,xmax=2,ymax=3]
      \tkzDrawXY[noticks=true]
      \draw [thick] plot [smooth, domain=1:1.7] (\x,{(\x -1)*(\x -3)*(\x
      -4) + 1 });
      \def\x{1.4}
      \def\xo{1.3}
      \draw (\xo,0) rectangle (\x,{(\x -1)*(\x -3)*(\x -4) + 1 });
      \draw [dashed] (0,{(\x -1)*(\x -3)*(\x -4) + 1 }) node [left]
      {$f(x_0 + h)$} -- (\x,{(\x -1)*(\x -3)*(\x -4) + 1 });
      \draw [dashed] (0,{(\xo -1)*(\xo -3)*(\xo -4) + 1 }) node [left]
      {$f(x_0)$} -- (\xo,{(\xo -1)*(\xo -3)*(\xo -4) + 1 });
      \draw (\xo,{(\xo -1)*(\xo -3)*(\xo -4) + 1 }) -- (\x,{(\xo
      -1)*(\xo -3)*(\xo -4) + 1 }) ;
    \end{tikzpicture}
  \end{minipage}

  On a supposé $f$ continue, on a donc $\lim_{h \to 0}f(x_0 +h) = f(x_0)$.
  On en déduit que $\underbrace{\lim_{h \to 0} \frac{\Phi(x_0 +
  h) - \Phi(x_0)}{h}}_{\Phi'(x_0)} = f(x_0)$.

  Par conséquent, $\Phi$ est dérivable en $x_0$ et $\Phi'(x_0) =f(x_0)$.
  $x_0$ étant quelconque dans $\intv{a}{b}$, on peut conclure quand à la
  dérivabilité sur $\intv{a}{b}$.
\end{proof}

\begin{remarque}
  \leavevmode
  \begin{itemize}
    \item Si la fonction était décroissante sur l'intervalle $\intv{a}{b}$,
      la démonstration est similaire.
    \item Si la fonction est croissante sur un nombre dénombrable
      d'intervalle de $I$ et décroissante sur un nombre dénombrable
      d'intervalle de $I$, on applique la démonstration aux intervalles où
      il y a monotonie et on vérifie que les dérivées coïncident.
  \end{itemize}
\end{remarque}

\section{Primitives}

\subsection{Définition et première propriétés}

On peut désormais généraliser le résultat précédent à toute fonction
continue.

\begin{definition}[Primitive]
  On appelle \emph{primitive} de $f$ sur $I$ une fonction $F$ telle que
  $F'=f$.
\end{definition}

\begin{remarque}
  On dit une primitive car l'unicité n'est pas garantie (et n'est pas
  nécessaire.)
\end{remarque}

\begin{proposition}
  Soit $f$ une fonction continue sur $I$ qui admet $F_1$ comme
  primitive. Alors pour tout $x \in I$, $F_2(x) = F_1(x) +k$ est aussi
  une primitive ($k\in\R$.)
\end{proposition}
\begin{proof}
  $F_1$ dérivable sur $I$ et $F_1' = f$. $F_2 \colon x\mapsto F_1(x) +
  k$ est dérivable et $F_2' = F_1'$

  Réciproquement, si $F_2$ est une primitive, $F_2' - F_1' = 0$ et donc
  $F_1 - F_2$ est une fonction constante. D'où le résultat.
\end{proof}

\begin{savoirfaire}[Trouver une primitive qui s'annule un point]
  On cherche une primitve de $f$ telle que $F(x_0) = 0$. D'après la
  proposition précédente, toutes les primitives de la forme $F(x) + k$
  conviennent. On va donc résoudre l'équation $F(x_0) + k = 0$ pour trouver
  la valeur de $k$.
\end{savoirfaire}

\begin{proposition}
  Toute fonction continue sur $I =\intv{a}{b}$ admet des primitives
  sur $I$.
\end{proposition}
\begin{proof}
  $f$ étant continue, d'après le théorème des valeurs intermédiaires,
  l'image de $f$ est un intervalle de la forme $\intv{m}{M}$, avec $m = \min
  f$ et $M = \max f$.\\
  La fonction $\phi \colon x \mapsto f(x) - m$ est donc positive, elle admet
  donc une fonction intégrale et donc une primitive.
\end{proof}

\begin{note}
  Le fait qu'une fonction admettent des primitives ne veut pas dire qu'on
  peut toujours les exprimer sous la forme de fonctions analytiques.
\end{note}

On vient de raisonner sur une fonction quelconque, \textit{i.e.} on a
rien supposé quant au signe. On peut donc généraliser la notion
d'intégrale d'une fonction de la façon suivante.

\begin{definition}
  On définit désormais l'intégrale ainsi :
  \begin{itemize}
    \item Si la fonction est positive sur $I$, alors l'intégrale
      $\int_a^b f(x) \diff{x}$ est l'aire sous la courbe (voir la
      définition \ref{def:int_pos})
    \item Si la fonction est négative sur $I$, alors l'intégrale
      $\int_a^b f(x) \diff{x}$ est l'opposé de l'intégrale de l'opposé.
      Autrement dit, on compte négativement l'aire d'une fonction
      négative.
    \item Si la fonction change de signe sur $I$ en $c$, on utilise la
      relation de Chasles et on se ramène aux deux cas précédents.
  \end{itemize}
\end{definition}

\begin{remarque}
  Attention : $\int_a^bf(x) \diff{x} = 0$ n'entraîne pas que $\forall x
  \in I,\ f(x) = 0$.
\end{remarque}

\begin{contreexemple}[Une fonction non nulle dont l'intégrale est nulle]
  Par un raisonnement géométrique, justifier que
  $\int_{-1}^1x\diff{x} = 0$. $f$ est-elle la fonction nulle ?
\end{contreexemple}

On déduit aisément de cette définition une propriété qui «généralise» un
résultat précédent.

\begin{proposition}
  Pour toute fonction $f$ continue, on a $\int_a^b -f(x)\diff{x} =
  -\int_a^b f(x)\diff{x}$.
\end{proposition}
\begin{proof}
  C'est l'extension au cas $\lambda < 0$ de la
  proposition~\ref{prop:linearite}.
\end{proof}

On peut désormais calculer une intégrale en passant par les primitives :
\begin{proposition}
  Pour tout fonction $f$ admettant une primitive explicite $F$, on a
  $\int_a^bf(x)\diff{x} = F(b) - F(a)$
\end{proposition}
\begin{proof}
  $F$ est la fonction «aire sous la courbe».
\end{proof}

\begin{remarque}
  On note souvent $\int_a^bf(x)\diff{x} = \left[ F(x) \right]_a^b$.
\end{remarque}

\begin{proposition}
  L'intégrale ne dépend pas de la primitive choisie.
\end{proposition}
\begin{proof}
  Soient $F$ et $G$ deux primitives.\\
  $\int_a^bf(x)\diff{x} = F(b) - F(a) = G(b) - G(a)$. Or, pour tout $x$
  réel, $F(x) = G(x) + k$. On a donc le résultat.
\end{proof}

\begin{proposition}
	\leavevmode
  \begin{itemize}
    \item Pour toutes fonctions $f$ et $g$ continues, $\int_a^bf(x) +
      g(x)\diff{x} = \int_a^bf(x) \diff{x} + \int_a^bg(x)\diff{x}$ ;
    \item pour toute fonction $f$ et pour tout $\lambda$ réel, $\int_a^b
      \lambda f(x) \diff{x} = \lambda \int_a^b f(x) \diff{x}$.
  \end{itemize}
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item $f+g$ possède une primitive, notons la $K$. De même, $f$ et $g$
      possède des primitives notées respectivement $F$ et $G$. On peut
      les exprimer : pour tout $x$ de $I$, on a, par exemple, $K(x) =
      \int_a^xf(t) + g(t)\diff{t},\ F(x) = \int_a^xf(t) \diff{t},
      \ G(x) = \int_a^xg(t)\diff{t}$ pour les primitives s'annulant en
      $a$. En dérivant, on a $K' = F' + G'$, ce qui entraîne que pour
      tout $x$ de $I$, $K(x) - F(x) - G(x) = k,\ k\in\R$. On trouve $k =
      0$ en prenant $x = a$ et donc on en déduit $\forall x\in I,\ K(x)
      = F(x) + G(x)$ et donc le résultat attendu.
    \item Laissée en exercice au lecteur\footnote{Mais le principe est
      le même.}.
  \end{itemize}
\end{proof}

Une conséquence de cette propriété est que l'opération de recherche de
primitive est «linéaire» : si $F$ et $G$ sont deux primitives de $f$ et
$g$, alors $F+\lambda G$ est une primitive de $f+\lambda g$.

\subsection{Calcul d'une primitive dans des cas «simples»}

On peut résumer dans le tableau suivant les résultats issus de la
«lecture inverse» du tableau des dérivées.

\renewcommand{\arraystretch}{2.1}

\begin{center}
  \begin{tabular}{|*{3}{>{\hfill}p{4cm}<{\hfill~}|}} \hline
    \textbf{Fonction $f:x\mapsto$} & \textbf{Intervalle de validité} &
    \textbf{Primitve $F:x\mapsto$} \\ \hline
    $0$            & $\R$ & $k \in\R$ \\ \hline
    $a,\ a\in\R$   & $\R$ & $ax + k,\ k\in\R$ \\ \hline
    $x$            & $\R$ & $\dfrac{x^2}2 + k,\ k\in\R$ \\ \hline
    $x^2$          & $\R$ & $\dfrac{x^3}3 + k,\ k\in\R$ \\ \hline
    $x^n,\ n\in\N$ & $\R$ & $\dfrac{x^{n+1}}{n+1} + k,\ k\in\R$ \\ \hline
    $\dfrac1{x^2}$ & $\R^*$ & $\dfrac{-1}{x} + k,\ k\in\R$ \\ \hline
    $\dfrac1x$ & $\R^*_+$ & $\ln{x} + k,\ k\in\R$ \\ \hline
    $e^x$ & $\R$ & $e^x + k,\ k\in\R$ \\ \hline
    $e^{ax},\ a\in\R^*$ & $\R$ & $\frac1a e^{ax} + k,\ k\in\R$ \\ \hline
    $\dfrac1{\sqrt{x}}$ & $\R_+^*$ & $2\sqrt{x} + k,\ k\in\R$ \\ \hline
  \end{tabular}
\end{center}

On utilise ensuite des propriétés comme l'additivité des primitives ou
la multiplication par un scalaire.

\begin{exercise}
  On considère une fonction polynomiale $f \colon x\mapsto ax^2 + bx +
  c$.
  \begin{enumerate}
    \item \begin{enumerate}
        \item Sur quel ensemble est définie cette intégrale ?
        \item Donner une primitive de cette fonction s'annulant en 0.
        \item Donner l'ensemble des primitives de $f$ sur $\R$.
      \end{enumerate}
    \item \begin{enumerate}
        \item Écrire l'intégrale de $f$ entre 0 et 1.
        \item Exprimer cette intégrale en fonction de $a$, $b$ et $c$.
      \end{enumerate}
  \end{enumerate}
\end{exercise}

\begin{remarque}
  Si la primitive d'une somme est bien la somme des primitives, ce n'est
  pas le cas pour le produit. L'exercice suivant pourra nous en
  convaincre.
\end{remarque}

\begin{contreexemple}[La primitive de $fg$ est différente de $FG$]
Soient $f\colon x\mapsto \frac1x$ et $g\colon x\mapsto
  x$ définie sur $I=\intv{1}{2}$.
  \begin{enumerate}
    \item \begin{enumerate}
        \item Donner une primitive de $f\times g$ sur $I$.
        \item Calculer $\int_1^2 f(x)g(x) \diff{x}$
      \end{enumerate}
    \item \begin{enumerate}
        \item Donner une primitive de $f$, puis de $g$ sur $I$.
        \item Calculer $\left(\int_1^2 f(x)\diff{x}\right) \times
          \left(\int_1^2 g(x)\diff{x}\right)$.
      \end{enumerate}
    \item Conclure.
  \end{enumerate}
\end{contreexemple}

\subsection{Calcul des primitives dans des cas plus «complexes»}

On vient de voir que le produit de deux fonctions ne disposait pas
nécessairement d'une primitive. Néanmoins, sous certaines conditions, on
peut écrire explicitement une primitive. Détaillons ces cas.

On considère ici que $u$ est une fonction définie au moins sur $I$ et
éventuellement présentant une certaines conditions.

\begin{center}
  \begin{tabular}{|*{3}{>{\hfill}p{4cm}<{\hfill~}|}} \hline
    \textbf{Fonction $f:x\mapsto$} & \textbf{Intervalle de validité} &
    \textbf{Primitve $F:x\mapsto$} \\ \hline
    $u'(x)u^n(x)$ & $\R$ & $\frac1{n+1}u^{n+1}(x) + k \in\R$ \\ \hline
    $\dfrac{u'(x)}{u^2(x)}$ & $\{x \mid u(x)\neq 0\}$ &
    $\dfrac{-1}{u(x)} + k,\ k\in\R$ \\ \hline
    $\dfrac{u'(x)}{u(x)}$ & $\{x \mid u(x) > 0\}$ &
    $\ln(u(x)) + k,\ k\in\R$ \\ \hline
    $\dfrac{u'(x)}{\sqrt{u(x)}}$ & $\{x \mid u(x) > 0\}$ &
    $2\sqrt{u(x)} + k,\ k\in\R$ \\ \hline
    $u'(x) e^{u(x)}$ & $\R$ & $e^{u(x)} + k,\ k\in\R$ \\ \hline
  \end{tabular}
\end{center}

\begin{exercise}
  Démontrer ces résultats.
\end{exercise}

La première ligne de ce tableau est particulièrement utile lorsqu'on
dispose d'une forme factorisée, par exemple issue d'une translation.

\begin{exercise}
  On considère la parabole de sommet $(1,1)$ s'annulant en $-1$ et en
  $1$.
  \begin{enumerate}
    \item Donner l'équation de cette parabole.
    \item Donner l'aire sous la courbe entre les valeurs $x = -1$ et $x
      = 1$.
    \item Que pensez-vous de l'aire sous la parabole $(x-2)^2 + 1$.
      Donner deux méthodes de calcul.
  \end{enumerate}
\end{exercise}

La dernière ligne du tableau nous permettra de donner un argument
(incomplet) à un des résultats du cours de probabilités. On peut dès à
présent s'en servir dans le cadre de l'exercice suivant.

\begin{exercise}
  Donner une primitive de la fonction $x\mapsto xe^{\frac{-x^2}2}$.
\end{exercise}

\begin{remarque}
  La fonction $x\mapsto e^{-\frac{x^2}2}$ ne possède pas de primitive
  explicite.
\end{remarque}

\begin{center}
  \includegraphics[width=0.75\linewidth]{differentiation_and_integration.png}
  %source : https://xkcd.com/2117/
\end{center}



%\section*{Réferences :}
%
%\begin{itemize}
%  \item \url{http://fonctionelliptique.pagesperso-orange.fr/Rectification.htm}
%  \item \url{http://fonctionelliptique.pagesperso-orange.fr/Quadrature.htm}
%  \item \url{http://www.cnrtl.fr/definition/rectification}
%  \item \url{http://serge.mehl.free.fr/anx/long_arc.html}
%\end{itemize}

