\chapter{Produit scalaire dans l'espace}

\section{Définition et premières propriétés}


\begin{definition}
	Soient deux vecteurs $\vv{u}: \begin{pmatrix}x\\ y\\ z\end{pmatrix}$ et $\vv{v}: \begin{pmatrix}x'\\y'\\z' \end{pmatrix}$ de l'espace, exprimés dans une même base dite	canonique. \\
	Le nombre réel $xx' + yy' + zz'$ est appelé produit
	scalaire de $\vv{u}$ et $\vv{v}$. Il est noté $\vv{u}\cdot\vv{v}$.
\end{definition}


\begin{remarque}
	\leavevmode
	\begin{itemize}
		\item L'expression du produit scalaire dépend de la base choisie.
		\item La base canonique, ou orthonormée, est la base telle que pour
		tout vecteur $\vv{e}$ de celle-ci, $\norm{\vv{e}} = 1$ et pour
		tout couple de deux vecteurs distincts de cette base, leur produit
		scalaire est nul.
		\item Les éléments qui «changent l'échelle des vecteurs» sont
		appelés les scalaires
	\end{itemize}
\end{remarque}

\begin{proposition}
	\[ \forall \vv{u} \in E,\ \vv{u}\cdot\vv{u} = \norm{\vv{u}}^2 \]
\end{proposition}

Si les vecteurs $\vv{u}$ et $\vv{v}$ ne sont pas colinéaires, il est
toujours possible de se placer dans le plan formé par ces deux vecteurs
et on a la propriété suivante.

\begin{proposition}
	Soit $M$ un point de l'espace.\\
	Dans le plan $(O,\vv{u},\vv{v})$, le produit scalaire égale
	le cosinus de l'angle orienté $(\vv{u},\vv{v})$.
\end{proposition}

On dispose toujours de la caractérisation suivante, fort utile en
pratique.

\begin{proposition}
	Soient $\vv{u}$ et $\vv{v}$ deux vecteurs de l'espace non nuls. \[
	\vv{u}\cdot\vv{v} = 0 \iff \vv{u} \perp \vv{v}. \]
\end{proposition}

La précision que les vecteurs $\vv{u}$ et $\vv{v}$ ne sont pas nuls est
importante. En effet, on rappelle les deux propriétés suivantes, qui
découlent de la définition.

\begin{proposition}
	\[ \forall (\vv{u},\vv{v})\in E^2,\ \vv{u}\cdot\vv{v} =
	\vv{v}\cdot\vv{u} \]
\end{proposition}

\begin{proposition}
	\[ \forall \vv{u} \in E,\ \vv{u}\cdot\vv{0} = 0 \]
\end{proposition}

Enfin, on rappelle une dernière proposition importante et également
utile en pratique, à savoir utiliser.


\begin{proposition}
	Soit $\vv{u}$ un vecteur de l'espace.\\
	Si, pour tout vecteur $\vv{v}$ du plan,
	$\vv{u}\cdot\vv{v} = 0$, alors $\vv{u} = 0$.
\end{proposition}

\section{Vecteur normal à un plan}

\subsection{Approche vectorielle}

\begin{note}[orthogonalité et colinéarité n'ont rien à voir dans l'espace]
	Si dans le plan, la notion d'orthogonalité permettait de caractériser
	les droites, dans l'espace, cette notion permettra de caractériser les
	plans. En effet, donnons nous un vecteur $\vv{u}: \begin{pmatrix}x\\ y\\ z\end{pmatrix}$. Les vecteurs $\vv{v}: \begin{pmatrix} 0\\ -z\\ y\end{pmatrix}$ et $\vv{w}: \begin{pmatrix}-z\\ 0\\ x\end{pmatrix}$	sont deux vecteurs orthogonaux à $\vv{u}$, mais ne sont pas colinéaires.
\end{note}


\begin{proposition}
	Une droite est orthogonale à toute droite du plan si et seulement si
	elle est orthogonale à deux droites sécantes de ce plan.
\end{proposition}
\begin{proof}
	Sens direct : On suppose qu'une droite est orthogonale à toutes les
	droites d'un plan. Elle est donc orthogonale à deux droites sécantes
	de ce plan.\\
	Sens réciproque : On suppose qu'une droite est orthogonale à deux
	droites sécantes du plan. Donc elle possède un vecteur directeur
	orthogonal à deux vecteurs non colinéaires du plan. Donc le vecteur
	directeur choisi est orthogonal à tout vecteur du plan, qu'on peut
	considérer comme vecteur directeur d'une droite. Donc la droite est
	orthogonale à toute droite du plan.
\end{proof}


\begin{definition}
	On dit que $\vv{u}$ est un vecteur normal au plan.
\end{definition}

\begin{proposition}
	La donnée d'un point et d'un vecteur normal définissent entièrement
	un plan de de l'espace.
\end{proposition}


\subsection{Approche «analytique»}

\begin{definition}
	Une équation de la forme $ax + by + cz + d =0$ est l'équation
	analytique d'un plan.\\
	Les coefficients en $x$, $y$ et $z$ sont les composantes
	du vecteur normal à ce plan, et $d$ est obtenu en multipliant terme
	à terme les composantes du vecteur normal et les coordonnées du
	point.
\end{definition}

\section{Quelques applications pratiques}

Il est en général pertinent de se poser la question de savoir quelle
représentation (analytique ou paramétrique) est la plus pertinente pour
un plan. Certaines questions, typiquement, l'appartenance d'un point à
un plan peuvent se traiter aussi bien avec l'une ou l'autre des
représentations du plan. En revanche, l'intersection de deux plans est
généralement aisée à traiter avec la représentation paramétrique, alors
que l'intersection de trois plans se traite davantage avec la
représentation analytique.

Parmi les autres applications, on peut citer les propriétés suivantes.

\begin{proposition}
	Deux plans parallèles ont des vecteurs normaux colinéaires
\end{proposition}

\begin{theoreme}[du toit]
	Si deux plans $\mathscr{P}$ et $\mathscr{P}'$ sont sécants selon une
	droite $\mathscr{D}$ et que $\mathscr{P}$ et $\mathscr{P}"$ sont
	sécants selon une droite $\mathscr{D}'$ parallèle à $\mathscr{D}$,
	alors $\mathscr{P}'$ et $\mathscr{P}"$ sont sécants selon une droite
	$\mathscr{D}"$ parallèle $\mathscr{D}$ et à $\mathscr{D}'$.
\end{theoreme}

\begin{center}
	\begin{tikzpicture}
	\tkzInit
	\tkzDefPoint(0,0){A}
	\tkzDefPoint(2,0){B}
	\tkzDefPoint(1,1){C}
	
	%\tkzDrawLines[add=1 and 1](A,B A,C B,C)
	\begin{scope}[shift=(45:3)]
	\tkzDefPoint(0,0){A'}
	\tkzDefPoint(2,0){B'}
	\tkzDefPoint(1,1){C'}
	\end{scope}
	%\tkzDrawLine[dashed, add=1 and 0](A',B')
	
	\end{tikzpicture}
\end{center}
