\chapter{Vecteurs dans l'espace}

\section{Généralités sur les vecteurs}

\subsection{Définition d'un vecteur de l'espace}

\begin{definition}
	Soient $A$ et $B$ deux points de l'espace. On dit que le vecteur 
	$\vv{AB}$ est le vecteur de la translation qui transforme $A$ en $B$.
\end{definition}

\begin{definition}
	Lorsque les points $A$ et $B$ sont confondus, on dit que le vecteur 
	$\vv{AA}$ est le vecteur nul. On le note $\vv{0}$.
\end{definition}

\begin{definition}
	Deux vecteurs $\vv{AB}$ et $\vv{CD}$ sont égaux si et seulement s'ils 
	forment un parallélogramme.\\
	Dans ce cas, on dit que $\vv{AB}$ et $\vv{CD}$ sont des représentants 
	d'un même vecteur $\vv{u}$.\\
	Pour tout point $E$ de l'espace et pour tout vecteur $\vv{v}$, il existe  
	un unique point $F$ tel que $\vv{v} = \vv{EF}$.
\end{definition}

\subsection{Opération sur les vecteurs de l'espace}

\begin{note}[Rappel]
	3 points arbitraires du plan non alignés forment un plan.
\end{note}

On mène les opérations sur les vecteurs de l'espace en se plaçant dans le 
plan formé par les points. On procède ainsi de la même façon que dans le 
plan.

\begin{note}[Règle du parallélogramme]
	Soient $\vv{u}$ et $\vv{v}$ deux vecteurs. Leur somme se construit en 
	appliquant la règle du parallélogramme.
	\begin{center}
		\begin{tikzpicture}[>=latex]
			\draw [thick,blue,->] (0,0) -- (2,3) ;
			\draw [blue] (1,1.5) node [left] {$\vv{u}$} ;
			\draw [dashed] (3,1) -- (5,4) ;
			\draw [thick,red,->] (0,0) -- (3,1) ;
			\draw [red] (1.5,0.5) node [below] {$\vv{v}$} ;
			\draw [dashed] (2,3) -- (5,4) ;
			\draw [thick,green,->] (0,0) -- (5,4) ;
			\draw [] (2.5,2) node [below right] {$\vv{u}+\vv{v}$} ;
		\end{tikzpicture}
	\end{center}
\end{note}

\begin{note}[Relation de Chasles]
	Soient $\vv{AC}$ un vecteur et $B$ un point. $\vv{AC} = \vv{AB} + \vv{BC}$
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [thick,red,->] (0,0) -- (3,1) ;
		\draw [red] (1.5,0.5) node [below] {$\vv{AB}$} ;
		\draw [thick, blue,->] (3,1) -- (5,4) ;
		\draw [blue] (4,2.5) node [right] {$\vv{BC}$} ;
		\draw [thick,green,->] (0,0) -- (5,4) ;
		\draw [] (2.5,2) node [above left] {$\vv{AB}+\vv{BC}$} ;
		\draw (0,0) node [below] {$A$} (3,1) node [below] {$B$} (5,4) node 
		[right] {$C$} ; 
		\end{tikzpicture}
	\end{center}
\end{note}

\begin{proposition}
	\leavevmode
	\begin{itemize}
		\item $\forall (k,k')\in\R^2$ et $\vv{u}$ un vecteur,
		$(k+k')\vv{u} = k\vv{u} + k'\vv{v}$ et $k(k'\vv{u}) = (kk')\vv{u}$.
		\item $\forall k\in\R$ et $\vv{u}$ et $\vv{v}$ deux
		vecteurs, $k(\vv{u} + \vv{v}) = k\vv{u} + k\vv{v}$.
		\item si $k=0$, $k\vv{u} = \vv{0}$ et $k \vv{u} = 0$ si $\vv{u} = 0$ ou 
		$k = 0$.
	\end{itemize}
\end{proposition}
\begin{proof}
	Voir cours de seconde.
\end{proof}

\section{Colinéarité et plan}

\subsection{Vecteurs colinéaires}

\begin{definition}
	Deux vecteurs $\vv{u}$ et $\vv{v}$ de l'espace sont colinéaires si et 
	seulement s'il existe un réel $\lambda ≠ 0$ tel que $\vv{v} = \lambda 
	\vv{u}$.
\end{definition}

\begin{remarque}
	Le vecteur nul est colinéaire à tout vecteur de l'espace.
\end{remarque}

\begin{proposition}
	\leavevmode
	\begin{itemize}
		\item Trois points $A$, $B$ et $C$ de l'espace sont alignés si et 
		seulement si les vecteurs $\vv{AB}$ et $\vv{AC}$ sont colinéaires.
		\item Les droites $(AB)$ et $(CD)$ sont colinéaires si et seulement si 
		les vecteurs $\vv{AB}$ et $\vv{CD}$ sont colinéaires.
	\end{itemize}
\end{proposition}
\begin{proof}
	Voir cours de seconde.
\end{proof}

\subsection{Plan et vecteurs colinéaires}

\begin{definition}
	Soient $A$, $B$ et $C$ trois points non alignés de l'espace. Le plan $\P$ 
	(aussi $(ABC)$) est l'ensemble des points $M$ de l'espace tels que 
	$\vv{AM} = x\vv{AB} + y\vv{AC}$
\end{definition}

\begin{remarque}
	On retrouve une définition analogue à celle d'une droite : la droite 
	$(AB)$ est l'ensemble des points $M$ de l'espace tels que $\vv{AM} = x 
	\vv{AB}$.
\end{remarque}

On peut désormais parler du plan $(A,\vv{u},\vv{v})$ comme le plan dirigé 
par les vecteurs $\vv{u}$ et $\vv{v}$ et passant par $A$.

\begin{proposition}
	Deux plans qui ont des vecteurs directeurs deux à deux colinéaires sont 
	parallèles.
\end{proposition}
\begin{proof}
	Soient $(A,\vv{u},\vv{v})$ et $(B,\vv{u'},\vv{v'})$ deux plans tels que 
	$\vv{u'} = \lambda \vv{u}$ et $\vv{v'} = \mu \vv{v}$.\\
	Les droites $(A,\vv{u})$ et $(B,\vv{u'})$ sont parallèles, de même que 
	les droites $(A,\vv{v})$ et $(B,\vv{v'})$, donc les plans sont parallèles.
\end{proof}

\section{Repérage dans l'espace}

\subsection{Points et vecteurs coplanaires}

\begin{definition}
	$\vv{u}$, $\vv{v}$ et $\vv{w}$ sont coplanaires si pour tout point $O$ de 
	l'espace, et pour $A,\ B$ et $C$ tels que $\vv{OA} = \vv{u}$, $\vv{OB} = 
	\vv{v}$ et $\vv{OC} = \vv{w}$ les points $O,\ A,\ B$ et $C$ sont dans le 
	même plan.
\end{definition}

\begin{proposition}
	Soient $\vv{u}$, $\vv{v}$ et $\vv{w}$ trois vecteurs tels que $\vv{u}$ et 
	$\vv{v}$ soient non colinéaires.\\
	$\vv{u}$, $\vv{v}$ et $\vv{w}$ sont coplanaires si et seulement s'il 
	existe des nombres réels $a$ et $b$ tels que $\vv{w} = a\vv{u} + b\vv{v}$.
\end{proposition}
\begin{proof}
	Soit $O$ un point de l'espace et $A,\ B$ et $C$ tels que $\vv{OA} = 
	\vv{u}$, $\vv{OB} = \vv{v}$ et $\vv{OC} = \vv{w}$. $\vv{u}$ et $\vv{v}$ 
	n'étant pas colinéaires, on peut considérer le plan $\P : 
	(O,\vv{u},\vv{v})$. Si $\vv{w}$ est coplanaire, $C \in \P$ et donc il 
	existe deux réels $a$ et $b$ tels que $\vv{OC} = a\vv{OA} + b\vv{OB}$.
\end{proof}

\begin{proposition}\label{geometrie:vecteurs:prop:decomposition}
	Soient $\vv{u}$, $\vv{v}$ et $\vv{w}$ trois vecteurs non coplanaires.\\
	Pour tout vecteur $\vv{t}$, il existe un unique triplet $(a,b,c)$ tel que 
	: \[ \vv{t} = a\vv{u} + b\vv{v} + c\vv{w} . \]
\end{proposition}
\begin{proof}
	\leavevmode
	\begin{description}
		\item[Existence] Soit $O$ un point et $\P$ le plan $(O,\vv{u},\vv{v})$. 
		On pose $\vv{t} = \vv{OM}$. La droite passant par $M$ parallèle à 
		$(O,\vv{w})$ coupe $\P$ en $M'$. On a donc, dans le plan 
		$(O,\vv{u},\vv{v})$ $OM' = a\vv{u} + b\vv{v}$.\\
		Comme $\vv{MM'} = c\vv{w}$, la relation de Chasles fournit le résultat.
		\item[Unicité] Supposons que $\vv{t} = a\vv{u} + b\vv{v} + c\vv{w} = 
		a'\vv{u} + b'\vv{v} + c'\vv{w}$. Si $c ≠ c'$, alors $\vv{w} = \frac{a' 
		- a}{c' - c} \vv{u} + \frac{b' - b}{c' - c}\vv{v}$, ce qui n'est pas 
		possible. On obtient donc $a\vv{u} + b\vv{v} = a'\vv{u} + b'\vv{v}$ ce 
		qui n'est pas non plus possible car $\vv{u}$ et $\vv{v}$ sont non 
		colinéaires.
	\end{description}
\end{proof}

\subsection{Repérage et coordonnées}

\begin{definition}
	Un triplet $(\vv{u},\vv{v},\vv{w})$ de vecteurs de l'espace non 
	coplanaire s'appelle une \textbf{base}.\\
	$(O,\vv{u},\vv{v},\vv{w})$ s'appelle un \textbf{repère} de l'espace.
\end{definition}

\begin{proposition}
	Dans la base $(\vv{\imath},\vv{\jmath},\vv{k})$, tout vecteur se 
	décompose de façon unique en $x\vv{\imath} + y\vv{\jmath} + z\vv{k}$. Le 
	triplet $(x,y,z)$ s'appelle les coordonnées du vecteur dans la base.\\
	Dans le repère $(O,\vv{\imath},\vv{\jmath},\vv{k})$ tout point $M$ se 
	décompose de façon unique en $\vv{OM} = x\vv{\imath} + y\vv{\jmath} + 
	z\vv{k}$. Le triplet $(x,y,z)$ s'appelle les coordonnées du vecteur dans 
	le repère.
\end{proposition}
\begin{proof}
	Voir la proposition~\ref{geometrie:vecteurs:prop:decomposition}.
\end{proof}

\begin{savoirfaire}[Calculs sur les coordonnées]
	Le calcul sur les coordonnées des vecteurs se mène de la façon suivante.
	\begin{multicols}{2}
		Soient $\vv{u}: \begin{pmatrix} x \\ y \\ z\end{pmatrix}$ et $\vv{v}: 
		\begin{pmatrix} x' \\ y' \\ z'\end{pmatrix}$.\\
		$\vv{u} + \vv{v} : \begin{pmatrix} x + x'\\ y + y' \\ z + 
		z'\end{pmatrix}$\\
		$\lambda \vv{u}: \begin{pmatrix} \lambda x \\ \lambda y \\ \lambda 
		z\end{pmatrix}$
		
		\columnbreak
		
		Si $A: (x_A;y_A;z_A)$ et $B: (x_B;y_B;z_B)$, alors 
		\begin{itemize}
			\item $I$ milieu de $[AB]$ a pour coordonnées $(\frac{x_A + x_B}{2} ; 
			\frac{y_A + y_B}{2} ; \frac{z_A + z_B}{2})$
			\item $\vv{AB} : \begin{pmatrix} x_B - x_A\\ y_B - y_A \\ z_B - 
			z_A\end{pmatrix}$
			\item $AB = \norm{\vv{AB}} = $\\$\sqrt{(x_B - x_A)^2 + (y_B - y_A)^2 
			+ 
			(z_B - z_A)^2}$
		\end{itemize}
	\end{multicols}
\end{savoirfaire}

\subsection{Représentations paramétriques}

\begin{proposition}
	Le système $(S) : \begin{cases} x = x_A + tu \\ y = y_A + tv \\ z = z_A 
	+ tw\end{cases}\!\! t \in \R$ est une représentation paramétrique de la 
	droite passant par $A$ de vecteur directeur $\begin{pmatrix} u \\ v \\ w	
	\end{pmatrix}$.
\end{proposition}
\begin{proof}
	Soit $M$ un point de la droite passant par $A$ de vecteur directeur 
	$\begin{pmatrix} u \\ v \\ w	\end{pmatrix}$. Supposons que $M$ a pour 
	coordonnées $M: (x,y,z)$ et $A: (x_A,y_A,z_A)$. D'après la définition 
	d'une droite, on a $\begin{pmatrix} x - x_A \\ y - y_A \\ z - z_A 
	\end{pmatrix} = t\begin{pmatrix} u \\ v \\ w	\end{pmatrix}, t \in\R$. On 
	en déduit que $\begin{cases} x - x_A = tu \\ y - y_A = tv \\ z - z_A = 
	tw\end{cases}\!\! t \in \R$, d'où le résultat.\\
	Réciproquement, si un point $M$ satisfait le système $(S)$, alors, les 
	étapes précédentes étant des équivalences, les vecteurs $\vv{AM}$ et 
	$\begin{pmatrix} u \\ v \\ w	\end{pmatrix}$ sont colinéaires.
\end{proof}

\begin{proposition}
	Le système $(S) : \begin{cases} x = x_A + tu + sa \\ y = y_A + tv + sb \\ 
	z = z_A + tw + sc \end{cases}\!\! (t,s) \in \R^2$ est une représentation 
	paramétrique du plan passant par $A$ de vecteur directeur 
	$\begin{pmatrix} u \\ v \\ w	\end{pmatrix}$ $\begin{pmatrix} a \\ b \\ c	
	\end{pmatrix}$.
\end{proposition}
\begin{proof}
	La démonstration est analogue à la précédente.
\end{proof}