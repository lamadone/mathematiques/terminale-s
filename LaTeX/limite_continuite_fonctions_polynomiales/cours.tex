%master: loi_probabilite.article.tex

\mode<article>{\usepackage{fullpage}}
\mode<presentation>{%
  \usetheme{Warsaw}
  \usecolortheme{beaver}

  \AtBeginSection[]
  {
    \begin{frame}
      \tableofcontents[currentsection]
    \end{frame}
  }
}
% everyone:
\pgfdeclareimage{logo}{Pictures/logo}

\title{Sur la continuité des fonctions polynomiales}
\author{Vincent-Xavier Jumel}
\institute{La Salle Saint-Denis}
\date{février 2021}
\logo{\pgfuseimage{logo}}

\begin{document}
\mode*

\begin{frame}
  \maketitle
\end{frame}

\begin{abstract}
  L'idée de ce cours est présenter rapidement quelques résutlats sur la
  continuité des fonctions usuelles. Essentiellement, la continuité repose
  sur l'idée qu'on ne lève pas son crayon pour tracer la représentation
  graphique de telles fonctions. Il s'agit d'un concept assez essentiel.
\end{abstract}

\section*{Quelques rappels}

\subsection*{Notations}

On rappelle les notations suivantes qui seront utilisés dans ce cours.

\begin{frame}
  \begin{enumerate}
    \item $\forall$, le quantificateur \alert{universel} indique que la
      proposition est vraie pour tous les éléments concernés. On le lit
      aussi «quelque soit».
    \item $\exists$, le quantificateur \alert{existentiel} indique
      l'existence d'au moins un élement qui vérifie la proposition. On le
      lit «il existe».
    \item $\exists!$ se lit «il existe un et un seul» et permet de mettre
      l'accent sur l'unicité d'un élément.
  \end{enumerate}
\end{frame}

\begin{frame}
  \begin{enumerate}
    \item $E$ désigne un ensemble de nombre, souvent une sous-partie de $\R$
    \item $F$ désigne également un ensemble de nombre, souvent une
      sous-partie de $\R$
    \item Lorsque $E$ ou $F$ sont des intervalles, on notera souvent $I$ ou
      $J$
  \end{enumerate}
  \note{On peut détailler à l'envi les notations communéments employées,
    mais la connaissance de $\R$ est largement suffisante. On peut, ensuite,
    généraliser à des ensembles plus «complexes» que $\R$, comme les espaces
  vectoriels normés comme $\R^d$, etc …}
\end{frame}
\begin{frame}
  \begin{enumerate}
    \item Lorsqu'on plusieurs éléments dont les rôles sont proches, on les
      numérote plutôt que de prendre des éléments consécutifs dans un
      alphabet. Ainsi, on peut écrire $x, y, z$, ou $x_1, x_2, x_3$, ou
      encore $x_0, x_1, x_2$
    \item $x$, $y$, $z$ sont des variables réelles
    \item $a$, $b$, $c$ sont des constantes (elles sont connues dans les
      applications concrètes)
  \end{enumerate}
\end{frame}

\subsection*{Vocabulaire}

\begin{frame}
  \begin{definition}[injectivité]
    On dit qu'une fonction $f \colon E \to F$ est injective lorsque, pour
    tout couple d'éléments $(x_1, x_2)$, $f(x_1) = f(x_2) \implies x_1
    = x_2$
  \end{definition}
\end{frame}

Autrement dit, si deux images sont égales, c'est en fait qu'elles ont les
mêmes antécédents.

\begin{frame}
  \begin{definition}[surjectivité]
    On dit qu'une fonction $f \colon E \to F$ est surjective lorsque, pour
    tout $y \in F$, il existe un élément $x$ de $E$ tel que $y = f(x)$.
  \end{definition}
  \begin{remarque}
    Il est «facile» de rendre une fonction surjective : il suffit de
    considérer la restriction de $f$ à l'ensemble image $f(E)$.
  \end{remarque}
\end{frame}

On note cette restriction $f_{|f(E)}$. Il convient aussi de noter que $f(E)$
est un ensemble.

\begin{frame}
  \begin{definition}[bijection]
    On appelle bijection une fonction qui est à la fois \alert{injective} et
    \alert{surjective}, c'est à dire telle que $\forall y \in F, \exists! x
    \in E,\ y = f(x)$
  \end{definition}
\end{frame}

\note{On peut démontrer que cette équivalence là}

\subsection*{Applications utiles}

On a souvent besoin de caractériser la proximité d'une variable et d'un
nombre donné. Pour cela, on a besoin d'une application de distance,
possédant trois propriétés :
\begin{enumerate}%[label=(\roman*)]
  \item positvité : $\forall (x,y) \in \R^2,\ d(x, y) ≥ 0$, avec $d(x,x) = 0$
  \item symétrie : $\forall (x,y) \in \R^2, d(x,y) = d(y,x)$
  \item inégalité triangulaire : $\forall (x,y) \in \R^2,\ d(x,y) ≤ d(x,0) +
    d(y,0)$
\end{enumerate}

\begin{frame}
  \begin{proposition}
    La valeur absolue $\left|\cdot\right|$ est une distance.
  \end{proposition}
\end{frame}

\section{Limite et continuité des fonctions}

\subsection{Limite}
Avant de commencer, il est important de donner quelques éléments sur la
notion de limite d'une fonction. Les programmes d'enseignement mettent
l'accent sur l'aspect séquentiel (avec des suites) des limites, qui
permettent aussi d'introduire la notion d'infini. On va tâcher ici de ne pas
y recourir.

On va d'ailleurs se donner une définition de l'infini, dans le contexte de
la droite réelle :

\begin{frame}
  \begin{definition}[Infini]
    On dit qu'une variable $x$ tend vers l'infini (noté $+\infty$) lorsque
    \[
      \forall A \in \R_+,\ \ x > A .
    \]
  \end{definition}
\end{frame}

De la même façon, pour indiquer qu'une quantité (souvent une distance) peut
être rendue aussi petite que l'on veut, on se fixe $\varepsilon > 0$ et on
imposer que la distance en question soit inférieure ou égale à
$\varepsilon$. On peut se représenter cet $\varepsilon$ comme un $10^{-n}$,
avec $n$ aussi grand que l'on veut. C'est d'ailleurs ainsi qu'informatique
on teste l'égalité de deux valeurs en virgule flottante, pour éviter les
erreurs d'arrondi.

\begin{frame}
  \begin{definition}[limite d'une fonction en un point de son domaine]
    Soit $f$ une fonction numérique réelle, définie sur $I$ un intervalle
    \alert{borné} de $\R$. On dit que $f$ admet $\ell$ pour limite en $a$ si
    \[
      \forall \varepsilon > 0,\ \exists \alpha_{\varepsilon} \in \R^*_+,\ \forall x \in
      I,\ \abs{x - a} ≤ \alpha_{\varepsilon} \implies \abs{f(x) - \ell} ≤ \varepsilon.
    \]
  \end{definition}
\end{frame}

On indique ici $\alpha_{\varepsilon}$ pour indiquer que le choix de $\alpha$ dépend du
choix de $\varepsilon$.

\begin{frame}
  \begin{tikzpicture}
    
  \end{tikzpicture}
\end{frame}

\subsection{Continuité}

\begin{frame}
  \begin{definition}[continuité]
    On dit qu'une fonction $f$ définie sur $I$ un intervale borné de $\R$
    est continue $a \in I$ si
     \[
      \forall \varepsilon > 0,\ \exists \alpha_{\varepsilon} \in \R^*_+,\ \forall x \in
      I,\ \abs{x - a} ≤ \alpha_{\varepsilon} \implies \abs{f(x) - f(a)} ≤ \varepsilon.
    \]
  \end{definition}
\end{frame}

Autrement dit, d'une part, la limite de $f$ en $a$ est précisément $f(a)$ et
d'autre part, des points proches de $a$, auront une image proche de l'image
de $a$.

\begin{frame}
  \begin{definition}
    On dit qu'une fonction définie sur $I$ est continue sur $I$ si elle est
    continue en tout point de $I$.
  \end{definition}
\end{frame}

\begin{frame}
  \begin{exemple}
    \begin{itemize}
      \item Les fonctions affines sont continues
      \item La fonction carré est continue
    \end{itemize}
  \end{exemple}
\end{frame}

\subsection{Opérations et continuité}

\begin{frame}
  \begin{proposition}
    Le produit d'une fonction continue par une constante réelle est continu.
  \end{proposition}
  \pause
  \begin{proposition}
    La somme de deux fonctions continues est continue.
  \end{proposition}
  \pause
  \begin{proposition}
    Le produit de deux fonctions continues est continu.
  \end{proposition}
\end{frame}

\begin{remarque}
  On dit que les fonctions continues forment un $\R$-espace vectoriel (les
  fonctions possèdent des propriétés similaires aux vecteurs) et même une
  $R$-algèbre.
\end{remarque}

\section{Applications directes de la continuité}

\end{document}
