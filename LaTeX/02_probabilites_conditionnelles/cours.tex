\chapter{Probabilités conditionnelles}

\section{Conditionnement}
\subsection{Probabilités conditionnelles}

Soit $A$ et $B$ deux parties non vides d'un espace de probabilité
$\Omega$. On admet, qu'alors, $\P(A)$ et $\P(B)$ sont tous deux différents
de 0.

\begin{definition}
  On appelle \emph{probabilité conditionnelle de $B$ sachant $A$} le
  nombre, noté $\P_A(B)$, $\dfrac{\P(A\cap B)}{\P(A)}$.
\end{definition}

\begin{remarque}
  On note aussi $\P(A|B)$.
\end{remarque}

\begin{proposition}
  On a aussi $\P(A\cap B) = \P_A(B) \times \P(A)$.
\end{proposition}

\subsection{L'arbre pondéré de probabilité}

L'arbre de probabilité correctement construit constitue une preuve !

En voici un exemple :

\begin{center}
  \begin{tikzpicture}[level distance=25mm,sibling distance=10mm]
    \node {} [grow=right]
    child[sibling distance=20mm] {
      node {$\overline{A}$}
      child[sibling distance=10mm] {
        node {$\overline{B} \longrightarrow 
        \P(\overline{A}\cap\overline{B})$}
        edge from parent node[below] {$\P_{\overline{A}}(\overline{B})$}
      }
      child[sibling distance=10mm] {
        node {$B \longrightarrow \P(\overline{A} \cap B)$}
        edge from parent node[above] {$\P_{\overline{A}}(B)$}
      }
      edge from parent node[below] { $\P(\overline{A})$ }
    }
    child[sibling distance=20mm] { node {$A$}
      child[sibling distance=10mm] {
        node {$\overline{B} \longrightarrow \P(A\cap\overline{B})$}
        edge from parent node[below] { $\P_A(\overline{B})$ }
      }
      child[sibling distance=10mm] {
        node {$B \longrightarrow \P(A\cap B)$}
        edge from parent node[above] { $\P_A(B)$ }
      }
      edge from parent node[above] { $\P(A)$ }
    } ;
  \end{tikzpicture}
\end{center}

Afin de retrouver la probabilité de $B$, on a besoin de la formule des
partitions. Précisons les termes employés.

\begin{definition}[Partition d'un ensemble]
  Soient $A_1,\dots,A_k$, $k$ sous ensembles d'un ensemble $\Omega$.

  On dit que $(A_i)_{1\leq i\leq k}$ forment une \emph{partition} de
  $\Omega$ si et seulement si
  \begin{itemize}
    \item $\bigcup_{i=1}^k A_i = \Omega$
    \item $\forall (i,j) \in [1,k]\cap\N,\  i\neq j \implies A_i\cap
      A_j = \emptyset$
  \end{itemize}
\end{definition}

\begin{remarque} $A$ et son complémentaire $\overline{A}$ forment une
  partition de $\Omega$.
\end{remarque}

\begin{remarque}
  Soient $A$ et $B$ deux parties de $\Omega$.

  Si $B \subset A$, alors $A\cap B = B$ et $A\cup B= A$.
\end{remarque}

\begin{proposition}[formule des partitions]
  Soit $(A_i)_{1\leq i\leq k}$ une partition de $\Omega$

  $\P(B) = \sum_{i=1}^k \P(A_i \cap B)$
\end{proposition}
\begin{proof}
	$B = (B\cap A_1)\cup(B\cap A_2)\cup … \cup (B \cap A_k)$. Or les $A_i$ 
	formant une partition de $\Omega$, ils partitionnent en particulier $B$ 
	et les $(B\cap A_i)$ sont deux à deux disjoints.
\end{proof}

On peut en particulier appliquer ceci à $A$ et son complémentaire. Ainsi, 
$\P(B) = \P(B\cap A) + \P(B \cap \overline{A})$.

\section{Indépendance}

\begin{proposition}
  Soient deux événements $A$ et $B$ de probabilités non nulles.
  $\P_B(A) = \P(A) \iff \P_A(B) = \P(B)$
\end{proposition}
\begin{proof}
	Si $\P_B(A) = \P(A)$, alors $\P(A) × \P(B) = \P(A\cap B)$ d'où le 
	résultat.
\end{proof}

On dit que deux événements sont indépendants, lorsque l'un ne
conditionne pas l'autre, c'est à dire que $\P_A(B) = \P(B)$.
On a donc le théorème suivant :

\begin{theoreme}[Indépendance]
  Soient $A$ et $B$ deux événements. $A$ et $B$ sont indépendants si et
  seulement si $\P(A\cap B) = \P(A)\times \P(B)$
\end{theoreme}

\begin{proposition}
  Soient $A$ et $B$ deux événements indépendants.\\
  Dans ce cas, $\overline{A}$ et $B$ sont aussi indépendants.
\end{proposition}

\begin{proof}
	Soient $A$ et $B$ deux événements indépendants. Alors $\P(A\cap B) = 
	\P(A)\times \P(B)$.\\ $\underbrace{\P((\overline{A} \cap B) \cup (A\cap 
	B))}_{\P(B)} = 	\P(\overline{A} \cap B) + \P(A)\times \P(B)$. On obtient 
	donc \\
	$\P(\overline{A}\cap B) = \P(B) - \P(A)×\P(B) = \P(B)(1-\P(A)) = 
	\P(\overline{A})×\P(B)$.
\end{proof}

\begin{corollaire}
	Si $A$ et $B$ sont indépendants, alors $\overline{A}$ et $\overline{B}$ 
	sont indépendants.
\end{corollaire}
\begin{proof}
	Le rôle tenu par $A$ et $B$ sont interchangeables dans la démonstration 
	précédente.
\end{proof}