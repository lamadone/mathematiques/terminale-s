\chapter{Limites des fonctions}

\section{Limite à l'infini}

\subsection{Limite finie}

\begin{definition}
	Soit $f$ une fonction définie sur $I$ un intervalle de $\R$. On dit que 
	$f$ admet pour limite $\ell$ en $+\infty$, notée $\lim_{x\to+\infty} f(x) 
	= \ell$ lorsque tout intervalle ouvert contenant $\ell$ contient toutes 
	les valeurs de $f(x)$	pour $x$ suffisamment grand.
\end{definition}

\begin{remarque} $x$ suffisamment grand signifie que $x \in 		
	\intv[o]{A}{+\infty}$ pour tout nombre $A > 0$.
\end{remarque}

\begin{exemple}
	La fonction $f \colon x \mapsto \frac{3x - 1}{x + 2}$ admet pour limite 3 
	en $+\infty$. On note $\lim_{x\to +\infty} f(x) = 3$.
	\begin{center}
		\begin{tikzpicture}
		\def\A{12.5}
		\draw [fill,green!15] (0,2.5) rectangle (15,3.2) ;
		\draw [black, very thick] (0,3) -- (15,3) ;
		\draw [thin, lightgray] (-1,-4) grid [step=1] (15,4) ;
		\draw [very thick, blue] plot [domain = -1:15,smooth]
		(\x,{(3*\x-1)/(\x+2)}) ;
		\draw [thick,->] (-1,0) -- (15,0) ;
		\draw [thick,->] (0,-4) -- (0,4) ;
		\foreach \x in {0,...,14} { \draw (\x,{(3*\x-1)/(\x+2)}) node
			[circle, fill, red!79!black, inner sep=1.5pt] {} ;
			\draw (\x,{(3*\x-1)/(\x+2)}) node [below right] {$u_{\x}$ } ;
		}
		\draw [green!95!black,very thick] (0,3) node [left] {$\mathbf{\ell}$} 
		-- (15,3) ;
		
		
		\end{tikzpicture}
	\end{center}
\end{exemple}

On définit de manière analogue les limites en $-\infty$. La condition $x$ 
suffisamment grand devient cependant $x$ suffisamment négatif, ce qui 
s'écrit $x \in \intv[o]{-\infty}{-A}$ pour tout nombre $A > 0$.

\begin{definition}
	Soit $f$ une fonction admettant $\ell$ pour limite en $\pm\infty$.\\
	La droite d'équation $y = \ell$ est une	\textbf{asymptote horizontale} à 
	la courbe représentative de la fonction	$\mathscr{C}_f$
\end{definition}

\subsection{Limite infinie}

\begin{definition}
	Soit $f$ une fonction définie sur $I$ un intervalle de $\R$. On dit que 
	$f$ admet pour limite $+\infty$ en $+\infty$ et on note 
	$\lim_{x\to+\infty} f(x) = +\infty$ lorsque tout intervalle de la forme 
	$\intv[o]{A}{+\infty}$ contient toutes les valeurs de $f(x)$ pour $x$ 
	suffisamment grand.
\end{definition}

\begin{exemple}
	La fonction $f \colon x \mapsto x^2$ a pour limite $+\infty$ en $+\infty$.
\end{exemple}

\subsection{Limite infinie en un point}

\begin{definition}
	Soit $f$ une fonction définie sur $I$ un intervalle de $\R$. On dit que 
	$f$ admet pour limite $+\infty$ en $a$ et on note $\lim_{x\to a} f(x) = 
	+\infty$ lorsque tout intervalle $\intv[o]{A}{+\infty}$ contient toutes 
	les valeurs de $f$ pour $x$ appartenant à un intervalle contenant $a$.
\end{definition}

On est souvent amené à distinguer les limites à gauche et à droite. On note 
la limite à gauche $\lim_{\substack{x\to a\\x<a}} f(x)$ et la limite à 
droite $\lim_{\substack{x\to a\\x>a}} f(x)$. Elles sont définies en 
remplaçant «intervalle contenant $a$» par «intervalle ouvert à gauche en 
$a$» et «intervalle ouvert à droite en $a$».

\begin{exemple}
	La fonction $f \colon x \mapsto \frac{-3x - 1}{-x + 2}$ admet pour limite 
	$+\infty$ en $2$. On note $\lim_{\substack{x\to 2\\x < 2}} f(x) = 
	+\infty$.
	
	\begin{center}
		\begin{tikzpicture}
		\def\a{2}
		\def\A{7}
		\def\e{0.7}
		
		\draw [thin, lightgray] (-5,-3) grid [step=1] (3,10) ;
		\draw [very thick, blue] plot [domain = -1.46:5,smooth]
		(-\x,{-(3*\x-1)/(\x+2)}) ;
		\draw [thick,->] (-5,0) -- (3,0) ;
		\draw [thick,->] (0,-3) -- (0,10) ;
		\foreach \x in {1,...,12} { \draw 
		(2-7/\x,{-(3*(-2+7/\x)-1)/((-2+7/\x)+2)}) node
			[circle, fill, green!79!black, inner sep=1.5pt] {} ;
			\draw (-7/\x+2,{-(3*(7/\x-2)-1)/((7/\x-2)+2)}) node [above left]
			{$u_{\x}$ } ;
		}
		\end{tikzpicture}
	\end{center}
\end{exemple}

\begin{definition}
	Soit $f$ une fonction admettant $\pm\infty$ pour limite en $a$.\\
	La droite d'équation $x = a$ est une \textbf{asymptote verticale} à la 
	courbe représentative de la fonction $\mathscr{C}_f$.
\end{definition}



\section{Opérations sur les limites}

\subsection{Limites en $\pm\infty$}

On peut ici reprendre le cours sur les suites et le relire en remplaçant
$u_n$ par $f(x)$, aussi bien pour les théorèmes de comparaisons que pour
les opérations.

\subsection{Limites finies}

Soient $f$ et $g$ deux fonctions, définies respectivement sur
$\mathcal{D}_f$ et $\mathcal{D}_g$, avec $f(\mathcal{D}_f)\subset
\mathcal{D}_g$.

On suppose que $\lim_{x\to a}f(x) = b$, avec $a\in\mathcal{D}_f$ et
$b\in\mathcal{D}_g$.

Alors $\lim_{x\to a}g(f(x))$ existe et vaut $g(b)$ ou la limite de $g$
en $b$.

\begin{remarque}
	Avec les hypothèses ci-dessus pour le domaine de définition et le
	domaine image, la fonction $x\mapsto g(f(x))$ se note $(g\circ f)(x)$
	ou plus simplement $g\circ f(x)$
\end{remarque}

\begin{exemple}
	$\lim_{x\to+\infty}\sin\frac{2}{\sqrt{x-2}}$.\\
	On calcule d'abord la limite de $\frac{2}{\sqrt{x-2}}$, qui vaut ici 0, 
	puis, comme $\sin 0 = 0$, on en déduit que 
	$\lim_{x\to+\infty}\sin\frac{2}{\sqrt{x-2}} = 0$
\end{exemple}
