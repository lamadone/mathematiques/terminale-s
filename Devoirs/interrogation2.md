# Interrogation flash n<sup>o</sup> 2

```{exercise}
Résoudre le système par combinaison linéaire (méthode du pivot de Gauss)

$$ \left\lbrace \begin{array}{l}
    4x - 3 y = 7\\
    6x + 2 y = 5
    \end{array}\right.
$$
```
