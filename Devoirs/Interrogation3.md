# Nombres complexes sous forme trignométrique

:::{exercise}
Déterminer une forme trigonométrique des nobres suivants :

1. $z_1 = 6i$
2. $z_2 = -\sqrt{3} + 3i$
3. $z_3 = -2 - 2i\sqrt{3}$
:::

:::{exercise}
Indiquer pourquoi les nombres suivants ne sont pas écrits sous forme
trigonométrique.

1. $z_1 = 3\left(\cos \frac{\pi}{12} - i\sin\frac{\pi}{12}\right)$
1. $z_2 = -2\left(\cos \frac{7\pi}{12} + i\sin\frac{7\pi}{12}\right)$
1. $z_3 = 4\left(\cos \frac{\pi}{4} + i\sin\frac{\pi}{3}\right)$
:::

:::{exercise}
Donner la forme algébrique du nombre

$$ z = 3\left(\cos \frac{8\pi}{3} + i \sin \frac{8\pi}{3}\right) $$
:::
