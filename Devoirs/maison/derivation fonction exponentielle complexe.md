# Devoir à la maison : dérivation de la fonction exponentielle complexe

:::{warning}
La qualité de la rédaction, la précision des réponses et la présentation
compteront pour une part importante de la note finale.

Toute trace de recherche, même incomplète doit figurer.
:::

::::{margin}
:::{note}
On admet qu'on peut définir des fonctions sur $\mathbf{R}$ à images dans
$\mathbf{C}$. On admet également que de telles fonctions sont dérivables
selon les règles de dérivation habituelles.

Le but de l'exercice est de démontrer que, pour tout $\theta \in
\mathbf{R}$, $\exp(i\theta) = \cos \theta + i\sin \theta$.
:::
::::

:::{exercise}
1. Rappeller les dérivées des fonctions
  * $\cos$
  * $\sin$
  * $t \mapsto t \exp(k t)$
2. En appliquant la question précédente, montrer que les fonctions $f \colon
   t \mapsto \exp (i t)$ et $g \colon t \mapsto \cos(t) + i \sin(t)$ sont
   telles que pour tout $t \in \mathbf{R},\ f'(t)$ s'exprime en fonction de
   $f(t)$ et $g'(t)$ s'exprime en fonction de $g(t)$.
3. En déduire que $f(t) = C*g(t)$, où $C$ est une constante.
4. Calculer $f(0)$ et $g(0)$ et en déduire la valeur de la constante $C$.
5. Conclure.
:::
