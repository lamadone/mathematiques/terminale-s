---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.12.0
kernelspec:
  display_name: Python 3.9.9 64-bit
  name: python3
---

# TP sur les ensembles de Julia

## Ensembles de Julia, ensembles de Mandelbrot

Une fractale est un objet mathématique dont la structure est  invariante par changement d'échelle.
```{margin}
Autrement dit, on peut «zoomer» et les parties auront toujours la même forme.
```

Dans ce TP, nous allons étudier les ensembles de Julia, nommés en l'honneur de Gaston Julia (XXè siècle), qui sont un exemple de fractales. Ces ensembles sont construits à partir de suites de nombres complexes récurrentes, définie par un premier terme $z_0$ et par la relation $z_{n+1} = z_n^2 + c$, où $c \in \mathbf{C}$. Ces suites sont soit bornées, soit divergentes, en fonction de la valeur initiale $z_0$.

```{margin}
Un ensemble de Julia est l'ensemble des valeurs de $z_0$ pour lesquels la suite est bornée.
```

Benoît Mandelbrot a décidé d'étudier les ensembles de Julia connexes. C'est à dire les ensembles pour lesquels, le choix de $c$ fait qu'il n'y a pas de «trous». On appelle de tels ensembles des ensembles de Mandelbrot.

## Représentation graphique d'un ensemble de Julia.

```{code-cell} ipython3
:tags: [remove-cell]

%matplotlib inline
import matplotlib.pyplot as plt
from random import random

def Julia(c):
    P = 1000000
    N = 100
    LX = []
    LY = []
    for i in range(P):
        z0 = random() * 4 - 2 + 1j*(random() * 4 - 2)
        z = z0
        k = 0
        while (abs(z) < 2 and k < N):
            k = k + 1
            z = z**2 + c
        if k == N:
            LX.append(z0.real)
            LY.append(z0.imag)
    return LX, LY
```
```{code-cell} ipython3
:tags: [remove-cell]

from myst_nb import glue
LX,LY = Julia(-1)
fig1, ax  = plt.subplots()
ax.scatter(LX,LY)
glue("figure1", fig1,display=False)

```

```{code-cell} ipython3
:tags: [remove-cell]
LX,LY = Julia(1/4)
fig2, ax = plt.subplots()
ax.scatter(LX,LY)
glue("figure2", fig2, display=False)

```
```{code-cell} ipython3
:tags: [remove-cell]
LX,LY = Julia(-0.12+0.74*1j)
fig3, ax = plt.subplots()
ax.scatter(LX,LY)
glue("figure3", fig3, display=False)
```


````{sidebar} Le programme
```ipython
%pylab inline
from random import random

def Julia(P,N):
    LX = []
    LY = []
    for i in range(P):
        z0 = random() * 4 - 2 + 1j*(random() * 4 - 2)
        z = z0
        k = 0
        while (abs(z) < 2 and k < N):
            k = k + 1
            z = z**2 - 1
        if k == N:
            LX.append(z0.real)
            LY.append(z0.imag)
    scatter(LX, LY)
    show()
```
````

On souhaite représenter l'ensemble de Julia pour $c = -1$ à l'aide du programme Python ci-contre. La fonction `Julia` possède le paramètre `P` qui correspond aux nombres de $z_0$ que l'on veut tester et `N`, qui correspond au nombre de terme de la suite $(z_n)_{n\in \mathbf{N}}$ qu'on va calculer pour chaque valeur de $z_0$. 

On admet que, s'il existe un entier $n_0$ tel que $|z_n| > 2$, alors $(z_n)$ n'est pas bornée.

On ne va donc conserver que les $z_0$ pour lesquels les $N$ valeurs de $z_n$ qu'on a calculé sont inférieures à 2.

1. `random()`  renvoie un nombre aléatoire entre 0 et 1. Que renvoie `random()*4 - 2` ?
2. Tester ce programme pour

    a. `P = 1000` et `N = 100`

    b. `P = 1000000` et `N = 100`

3. Modifier ce programme pour représenter les ensembles de Julia lorsque :

    a. $c = 0,25$

    b. $c = -0,12 + 0,74i$

::::{margin}
:::{tip}
En Python, $i$ s'écrit `1j`.
:::
::::

```{code-cell} ipython3
:tags: [margin, tip]

(1j)**2 == -1
```

`````{grid}
````{grid-item}
```{glue:figure} figure1
:name: "Julia-1"

L'ensemble de Julia pour $c = -1$
```
````
````{grid-item}
```{glue:figure} figure2
:name: "Julia+0.25"

L'ensemble de Julia pour $c = \frac14$
```
````
````{grid-item}
```{glue:figure} figure3
:name: "Julia-c"

L'ensemble de Julia pour $c = -0,12 + 0,74i$
```
````
`````
