---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.12.0
kernelspec:
  display_name: Python 3.9.9 64-bit
  name: python3
---

# Controle

## Exercice 0 Matrice
Effectuer le produit de matrice $A × B$ avec
    $A = \begin{pmatrix}
      1 & 2 & 4 & 3 \\
      2 & 4 & 3 & 1 \\
      4 & 3 & 1 & 2 \\
      3 & 1 & 2 & 4
    \end{pmatrix}$
et
    $B  = \begin{pmatrix}
      0 & 1 & 0 & 1 \\
      1 & 1 & 0 & 0 \\
      1 & 0 & 0 & 1 \\
      0 & 0 & 1 & 1
    \end{pmatrix}$


## Exercice 1

1. Soit l'équation $(E) : 109x - 226y = 1$ où $x$ et $y$ sont des entiers
    1. Déterminer PGCD(109, 226). Que peut-on en conclure pour l'équation $(E)$.
    2. Montrer que l'ensemble des solutions de $(E)$ est l'ensemble des couples
       de la forme $141 + 226k, 68 + 109k$, où $k \in \mathbf{Z}$.
    3. En déduire qu'il existe un unique couple $(d,e)$ d'entiers tels que $d
       ≤ 226$ et $109d = 1 + 226e$.
       Donner les valeurs de $d$ et $e$.
2. Démontrer 227 est premier.
3. On note $A$ l'ensemble des entiers naturels $a$ tels que $a ≤ 226$.
   On définit deux fonctions $f$ et $g$ de $A$ dans $A$ telles que
   + $f$ associe à $a$ le reste de la division euclidienne de $a^{109}$ par
   227
   + $g$ associe à $a$ le reste de la division euclidienne de $a^{141}$ par
   227

   1. Vérifier que $g[f(0)] = 0$
   2. Montrer que pour tout $a\in A$ non nul, $a^{226} \equiv 1 \mod 227$.
   3. En utilisant 1.2, déduire que, pour tout $a\in A$ non nul, $g[f(a)] =
      a$.
   4. Que peut-on dire de $f[g(a)]$ ?
      Comment sont $f$ et $g$ l'une par rapport à l'autre ?

## Exercice 2

Pour transmettre un message, on utilise le système suivant.
+ **1ère étape** À chaque lettre du message en clair, on associe son numéro
  d'ordre dans l'alphabet :

  A -> 01, B -> 02, C -> 03, … Y -> 25, Z -> 26.

  On obtient ainsi une suite de nombres.
+ **2ème étape** On considère la suite d'entiers naturels $(x_n)$ définie
  par
      $$\left\{\begin{array}{l}
      x_1 = 1 \\x_{n+1} \equiv 5x_n + 2 \mod 33
      \end{array}\right.$$
+ **3ème étape** On ajoute terme à terme les suites obtenues dans la
  première et la deuxième étape : on a alors le message chiffré.

1. Déterminer les 25 premiers termes de la suite $x$.
2. Coder le message suivant :
   `DEBARQUEMENTLEHUITJUIN`
3. Décoder le message suivant :
    `5,12,24,37,34,21,10,19,27,34,13,8,26,27,16,23,22,25,41`
