---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Récréation analytique

Le but de cet exercice est d'établir les inégalités d'Hölder et de
Minkowski.

1. Montrer, par un calcul simple, que pour tout $x$ et $y$ réels positifs,

    $$ xy ≤ \frac12 (x^2 + y^2) $$

2. On cherche désormais à généraliser cette relation. Soient $p$ et $q$ deux
nombres réels positifs tels que $\frac1p + \frac1q = 1$.
    1. Montrer que $p > 1$.

        ::::{margin}
        :::{tip}
        :class: dropdown
        On peut étudier, pour $y$ fixé la fonction $x\mapsto \frac1p x^p +
                \frac1q y^q - xy$.
        :::
        ::::

    2. On suppose $p > 1$. Montrer que, pour tout $x$ et $y$ réels positifs,

        $$ xy ≤ \frac1p x^p + \frac1q y^q.$$

3. Soient $(p,q) \in (\mathbf{R}_+)^2$, tels que $\frac1p + \frac1q = 1$.
Soient $x_1, x_2, … x_n, y_1, y_2, y_n$ des réels, $n \in \mathbf{N}$.
    1. Montrer que, pour tous réels $t_1, t_2, …, t_n$,

        $$|t_1 + t_2 + … + t_n | ≤ |t_1 | + |t_2| + … + |t_n | $$

    2. Montrer que

        $$|x_1y_1 + x_2y_2 + … + x_ny_n | ≤ \frac{|x_1|^p + |x_2|^p + … + |x_n |^p}p +  \frac{|y_1 |^q + |y_2|^q + … + |y_n |^q}q $$

    3. Soit $\lambda \in \mathbf{R}_+^*$. Que devient l'égalité précédente
       si on remplace tous les $x_i$ par $\lambda x_i$ et $y_i$ par
       $\frac1{\lambda} y_i$.

        ::::{margin}
        :::{tip}
        :class: dropdown
        Choisir $\lambda$ pour avoir $\lambda^p(|x_1|^p + … + |x_n|^p) =
                \frac1{\lambda^q}|y_1|^q + … + |y_n|^q)$.
        :::
        ::::

    4. Choisir judicieusment $\lambda$ pour avoir

        $$|x_1y_1 + x_2y_2 + … + x_ny_n | ≤ (|x_1|^p + |x_2|^p + … + |x_n |^p)^{\frac1p} × (|y_1 |^q + |y_2|^q + … + |y_n |^q)^{\frac1q} $$

    Cette dernière égalité est appellée inégalité de Hölder.

4. Soient $x_1, …, x_n, y_1, …, y_n$ des réels où $n \in \\N^*$ et $p \in ]1,
+\infty[$.
    1. Montrer que, pour tout $k \in \left[ 1, n \right]$,
       $|x_k + y_k|^p ≤ |x_k| |x_k + y_k|^{p-1} + |y_k| |x_k + y_k|^{p-1}$.

        ::::{margin}
        :::{tip}
        :class: dropdown
        Utiliser l'inégalité de Hölder avec $q = \frac{p}{p-1}$ et les réels
        bien choisis
        :::
        ::::

    2. Montrer que :

        $$ |x_1| |x_1 + y_1|^{p-1} + … + |x_n| |x_n + y_n|^{p-1} ≤ (|x_1|^p
            + |x_2|^p + … + |x_n |^p)^{\frac1p}(|x_1 + y_1|^p + |x_2 + y_2|^p
              + … + |x_n + y_n |^p)^{1 - \frac1p} $$

        et

        $$ |y_1| |x_1 + y_1|^{p-1} + … + |y_n| |x_n + y_n|^{p-1} ≤ (|y_1|^p
            + |y_2|^p + … + |y_n |^p)^{\frac1p}(|x_1 + y_1|^p + |x_2 + y_2|^p
              + … + |x_n + y_n |^p)^{1 - \frac1p} $$

    3. En déduire l'inégalité de Minkowski :

        $$ (|x_1 + y_1|^p + … + |x_n + y_n|^p)^{\frac1p} ≤ (|x_1|^p + … + |x_n|^p)^{\frac1p} + (|y_1|^p + … + |y_n|^p)^{\frac1p} $$



