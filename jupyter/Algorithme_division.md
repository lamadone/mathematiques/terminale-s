---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.12.0
kernelspec:
  display_name: Python 3.9.9 64-bit
  name: python3
---

# Un algorithme pour la division entière

```{code-cell} ipython3
def division(a: int, b: int) -> (int, int):
    e = 1
    while b*e <= a:
        e = e + 1
    q = e - 1
    r = a - b*q
    return (q,r)
```

```{code-cell} ipython3
division(114, 8)
```
