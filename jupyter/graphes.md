---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Graphes

## Définition

:::{proof:definition} Graphe non orienté
Un **graphe non orienté** $G$ est le couple $G = (S, A)$ où $S$ est un
ensemble de sommets et $A$ un ensemble d'ensembles à deux éléments sur $S$.
:::

::::{proof:exemple} Exemple
* $G = (ø, ø)$ est le graphe vide
* $G = (\{1\}, ø)$ est un graphe avec un seul sommet
* $G = (\{1, 2, 3\}, \{\{1,2\}, \{2,3\}, \{3,1\}\})$ est le graphe
    :::{mermaid}
    graph LR;
        A((1)) --- B((2)) ;
        B --- C((3)) ;
        C --- A ;
    :::
::::

:::{exercise}
Représenter le graphe $G = (\{1,2,3,4\}, \{\{1,2\}, \{1,3\}, \{1,4\}\})$
:::

:::{proof:definition} Ordre d'un graphe
L'**ordre** d'un graphe est le cardinal de $S$, c'est à dire le nombre de
sommet
:::

:::{proof:definition} adjacence
* Deux sommets sont dits **adjacents** s'ils sont reliées par une arête.
* Un somme adjacent à aucun autre est dit **isolé**.
:::

:::{proof:definition} degré
Le **degré** d'un sommet est le nombre d'arêtes connectées à ce sommet.
:::

:::{proof:definition} graphe complet
Un graphe est dit **complet** lorsque tout ses sommets sont adjacents entre
eux.
:::

:::{proof:definition} graphe orienté
Un **graphe orienté** $G$ est le couple $G = (S, A)$ où $S$ est un
ensemble de sommets et $A$ un ensemble de couple sur $S$.
:::
:::{margin}
Dans le cas d'un graphe orienté les arêtes se nomment **arcs**.
:::

::::{proof:exemple} Exemple de grahe orienté
* $G = (\{1, 2, 3\}, \{(1,2), (2,3), (3,1)\})$ est le graphe
    :::{mermaid}
    graph LR;
        A((1)) --> B((2)) ;
        B --> C((3)) ;
        C -->A ;
    :::
::::
:::{margin}
Dans un graphe orienté, un nœud (ou sommet) peut être relié à lui même.
:::

## Parcours dans un graphe

:::{proof:definition} Chaine (ou chemin)
Dans un graphe non orienté (resp. orienté), une **chaine** (resp. un
    **chemin**) de longueur $n$ est une succession de $n$ arêtes (resp.
      arcs) telles que l'extrémité de chacune est l'origine de la suivante
:::
::::{proof:exemple}
Dans le graphe
:::{mermaid}
graph LR;
    A((A)) --- E((E)) --- C;
    A --- D((D)) --- E;
    A --- C((C)) ;
    B((B)) ;
:::
* $B$ est un sommet isolé, de degré 0
* $A$ et $E$ sont des sommets de degré 3
* $C$ et $D$ sont des sommets de degré 2
* $A-D-E-A-C$ est une chaine de longeur 5
::::

:::{proof:definition} Chemin (ou chaine) fermé(e) et cycle (ou circuit)
* Si le dernier sommet d'un chemin (ou d'une chaine) est le même que le
premier, on dit que le chemin (ou le chaine) est **fermé(e)**.
* Si un chemin (ou une chaine) ne comporte pas de répétition, on dit que
c'est un **cycle** (ou **circuit**).
:::

:::{proof:exemple}
Dans l'exemple ci dessus, $A-D-E-C-A$ est une chaine fermée de longueur 4,
     c'est même un cycle de longueur 4.
:::

::::{proof:exemple}
:::{mermaid}
graph LR
    A((A)) --> A ;
    A --> B((B)) --> D((D)) ;
    A --> E((E)) --> F((F)) --> F ;
    E --> D ;
    E --> C((C)) --> A ;
:::

| Nœud | $A$ | $B$ | $C$ | $D$ | $E$ | $F$ |
| :--- | --- | --- | --- | --- | --- | --: |
| Degré|  5  |  2  |  2  |  2  |  4  |  3  |

* $A$ et $F$ comptent une boucle, comptée deux fois dans le degré.
* $A-E-D$ est un chemin de longueur 3 reliant $A$ à $D$.
* $E-C-A-A-E$ est un circuit de longueur 4.
* $E-C-A-E$ est un chemin de longueur 3.
::::

:::{proof:proposition}
Dans un graphe, la somme des degrés de chaque sommet est égale au double du
nombre d'arête.
:::

:::{exercise}
Vérifier cette proposition sur les exemples précédents
:::

::::{exercise}
On considère le graphe orienté suivant.
:::{mermaid}
graph LR
    A((A)) --> E((E)) --> D((D)) --> D --> C((C)) --> B((B)) --> B --> G((G)) --> F((F)) --> F --> E
    A --> F
    G --> A
    C --> A
    B --> A
    D --> A
:::
1. Déterminer l'ordre du graphe, ainsi que le degré de chaque sommet.
2. En déduire, par un calcul, le nombre d'arcs de ce graphe.
3. Déterminer un chemin de longueur 5 reliant $G$ à $C$.
4. Déterminer un circuit d'origine $A$.
::::

:::{exercise}
Construire un graphe non orienté d'ordre 4, composé de 3 arêtes. Donner le
degré de chaque sommet.
:::

## Matrice d'adjacence

:::{proof:definition} Matrice d'adjacence
À tout graphe $G$, oriené ou non, d'ordre $p$, dont les sommets sont notés
$s_1, …, s_p$, on peut associer une matrice $M \in \mathcal{M}_p(\mathbf{R})$ telle que
$m_{ij}$ est le nombre d'arcs ou d'arêtes reliant les sommets $s_i$ et
$s_j$.
On dit alors que $M$ est la **matrice d'adjacence**.
:::

::::{proof:exemple}
:::{mermaid}
graph LR
    A((A)) --> A ;
    A --> B((B)) --> D((D)) ;
    A --> E((E)) --> F((F)) --> F ;
    E --> D ;
    E --> C((C)) --> A ;
:::
a pour matrice d'adjacence associée la matrice

$$ \begin{pmatrix}
1 & 1 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 & 0 & 0 \\
1 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 1 & 1 & 0 & 1 \\
0 & 0 & 0 & 0 & 0 & 1 \\
  \end{pmatrix} $$

::::

### Nombres de chemins de longueur $n$

:::{proof:theorem}
Soit $G$ un graphe orienté d'ordre $p$ (de sommets $s_1, … s_p$) et $M$ sa
matrice d'adjacence d'ordre $p$. Si on note $M^n = (m^{(n)}_{ij})$, alors
$m^{(n)}_{ij})$ est le nombre de chemin de longueur $n$ reliant $i$ à $j$.
:::
:::{proof:proof}
Par récurrence.
:::
:::{exercise}
:label: nbre_chemins
Pour la matrice précédente, donner le nombre de chemin de longueur 5 reliant
$E$ à $F$.
:::
:::{solution} nbre_chemins

$$M^5 = \begin{pmatrix}
4 & 3 & 2 & 4 & 3 & 5\\
0 & 0 & 0 & 0 & 0 & 0\\
3 & 2 & 1 & 2 & 2 & 3\\
0 & 0 & 0 & 0 & 0 & 0\\
2 & 1 & 1 & 2 & 1 & 3\\
0 & 0 & 0 & 0 & 0 & 1\\
end{pmatrix}$$

On trouve donc 3
:::

::::{exercise}
On considère le graphe orienté suivant.
:::{mermaid}
%%{init: {"flowchart":{"curve":"linear"}}}%%
graph LR
    A((A)) --> E((E))
    E --> D((D)) --> D
    D --> C((C))
    C --> B((B)) --> B
    B --> G((G))
    G --> F((F)) --> F
    F --> E
    A --> F
    G --> A
    C --> A
    B --> A
    D --> A
:::
1. Déterminer une matrice d'adjacence $M$ de ce graphe.
2. Combien y-a-t-il de chemins de longueur 6 reliant $D$ à $B$ ?
::::

::::{exercise}
On considère le graphe non orienté suivant.
:::{mermaid}
%%{init: {"flowchart":{"curve":"linear"}}}%%
graph LR
    A((A)) --- B((B))
    A --- D((D))
    A --- F((F))
    B --- C((C))
    B --- E((E))
    C --- G((G))
    C --- E
    D --- E
    D --- F
    E --- G
    F --- G
:::
1. Déterminer une matrice d'adjacence $M$ de ce graphe.
2. Combien y-a-t-il de chaines de longueur 10 reliant $E$ à $F$.
::::


