# Matrices

## Définition

Une matrice est un tableau de nombres réels ou complexes (ou même éventuellement relatifs)

$$A  = \begin{pmatrix}
    a_{11} & a_{12} & a_{13} & \dots & a_{1p} \\
    a_{21} & a_{22} & a_{13} & \dots & a_{2p} \\
    \vdots & \vdots &        &       & \vdots \\
    a_{n1} & a_{n2} & a_{n3} & \dots & a_{np} \\
\end{pmatrix}$$

Dans cette matrice, on note aussi $A = (a_{ij})_{\substack{1\leq i \leq
  n\\1\leq j \leq p}}$, où $a_{ij}$ représente le coefficient situés à la
  $i$-ème ligne et la $j$-ième colonne.

Une telle matrice comporte $n$ lignes et $p$ colonnes et on dit que $A \in
\mathcal{M}_{n,p}(\mathbf{R})$.

```{proof:definition}
+ Si le nombre de colonnes est 1, on parle de matrices colonne.
+ Si le nombre de lignes est 1, on parle de matrices ligne.
+ Si le nombre de lignes égale le nombre de colonnes, on dit que la matrice
est carrée et on note $\mathcal{M}_{n}(\mathbf{R})$.
```

## Opérations

:::{proof:proposition} Somme et produit par un réel
Soient $A = (a_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ et $B = (b_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ deux matrices de $\mathcal{M}_{n,p}(\mathbf{R})$ et $\lambda$ un réel.

Alors $A + \lambda B =  (c_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ est une
matrice de $\mathcal{M}_{n,p}(\mathbf{R})$ et pour tout $i$ et pour tout
$j$, $c_{ij} = a_{ij} + \lambda b_{ij}$.
:::

:::{proof:proposition} Produit de deux matrices
Soient $A = (a_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ et $B = (b_{ij})_{\substack{1\leq i \leq p\\1\leq j \leq q}}$ deux matrices de $\mathcal{M}_{n,p}(\mathbf{R})$ et $\mathcal{M}_{p,q}(\mathbf{R})$.

Alors $A × B =  (c_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq q}}$ est une
matrice de $\mathcal{M}_{n,q}(\mathbf{R})$ et pour tout $i$ et pour tout
$j$, $c_{ij} = \sum_{k=1}^p a_{ik} b_{kj}$.
:::

:::{proof:proposition}
+ Le produit de deux matrices est associatif
+ Le produit est distributif sur l'addition
+ $(0)_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ est absorbant (avec les
    bonnes dimensions.
:::

## Cas des matrices carrés.

Une façon de limiter les soucis sur les matrices est de ne considérer que
des matrices carrées, c'est à dire où $n = p$.

:::{proof:definition} Identité
Il existe une matrice $I_n$ telle que, pour tout matrices carré $A$ de $\mathcal{M}_{n}(\mathbf{R})$, $AI_n = I_nA = A$.

$I_n$ s'appelle la «matrice identité» et

$$I_n = \begin{pmatrix}
    1 & 0 & 0 & … & 0 \\
    0 & 1 & 0 & … & 0 \\
    0 & 0 & 1 & … & 0 \\
    0 & 0 & 0 & … & 1 \\
\end{pmatrix}$$

:::

:::{proof:definition} Inverse d'une matrice
On dit qu'une matrice est inversible s'il existe une matrice $B$ telle que
$AB = I_n$.

$B$ est alors notée $A^{-1}$.
:::

Dans ce cas, on remarque que $BA = I_n$.

:::{proof:definition} Déterminant d'une matrice
Soit $A = \begin{pmatrix} a & b \\ c & d \end{pmatrix}$ une matrice. On note
$\det A$ son déterminant et

$$\begin{vmatrix} a & b \\ c & d \end{vmatrix} = ad - bc$$
