# Cours de mathématiques expertes (Terminale)

Le cours de mathématiques expertes est un enseignement optionnel qui
s'adresse à de bons élèves curieux souhaitant développer et approfondir
leurs connaissances et capacités, en vue d'une poursuite d'étude
scientifique.
Le programme est référencé sur la [page Éduscol relative à l'enseigement des
mathématiques au lycée](https://eduscol.education.fr/1723/programmes-et-ressources-en-mathematiques-voie-gt) et plus précisément sur le [B.O. spécifique](https://eduscol.education.fr/document/24574/download)



```{tableofcontents}
```

