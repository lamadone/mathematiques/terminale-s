# Les défis de la Renaissance

## Équations du second degré

À partir des travaux des mathématiciens perses, on a pu obtenir des méthodes
géométriques de résolution des équations algébriques du second degré. Ces
méthodes fonctionnent pour certaines classes d'équations, et on peut se
poser la question de quitter le champ de la géométrie pour passer à celui de
l'algèbre.

```{exercise}
Illustrer graphiquement la résolution d'une équation du second degré, par
exemple $x^2 + 4x = 21$
```

```{exercise}
Résoudre les équations suivantes :
+ $x^2 - 3x + 2 = 0$
+ $x^2 + 6x - 12 = 0$
+ $x^2 + 4x = 25$
```

```{exercise}
On considère l'équation $(E)\ ax^2 + bx + c = 0$.
1. Démontrer pourquoi la quantité $\Delta = b^2 - 4ac$ discrimine le nombre
   de solutions de l'équation $(E)$.
2. Donner les solutions générales de $(E)$.
```

```{exercise}
1. On considère un polynôme du second degré sous la forme $X^2 - SX + P$.
   1. Donner une condition sur $S$ et $P$ pour que le polynôme possède deux racines réelles.
   2. On note $X_1$ et $X_2$ les deux racines. Exprimer $S$ et $P$ en fonction de $X_1$ et $X_2$.
2. Trouver deux nombres dont la somme est 42 et le produit est 42.
```

## Équation du troisième degré

```{exercise}
Donner le développement de $(X - a)^3$.
```

```{exercise}
On considère le polynôme $2 x^{3} - 11 x^{2} + 18 x - 9$ dont on
cherche les racines, c'est à dire les solutions de l'équation $2 x^{3} - 11 x^{2} + 18 x - 9= 0$.
1. Vérifier que 1 est une racine de ce polynôme.
2. 
   1. Justifier que 1 est aussi une racine du polynôme $(x - 1)(ax^2 + bx + c)$
   2. En développant ce polynôme et en l'identifiant à $2 x^{3} - 11 x^{2} + 18 x - 9$ donner la valeur de $a$, $b$ et $c$.
   3. Résoudre l'équation $2 x^{2} - 9 x + 9 = 0$
   4. Donner l'ensemble des racines.
      
      Que constate-t-on ?
3. Écrire le polynôme $2 x^{3} - 11 x^{2} + 18 x - 9$ sous forme
      factorisée.
```

```{exercise}
On cherche une méthode générale pour factoriser les polynômes de degré 3.

On considère un polynôme de degré 3 qui s'écrit $P = X^3 + bX^2 + cX + d$.

1. On pose $X = Y - \frac{b}3$. Montrer qu'alors $P = Y^3 + pY + q$.

   Préciser les valeurs de $p$ et $q$.
2. 
   1. Résoudre l'équation $P = 0$ dans le cas où $p = 0$. Préciser la
      condition alors sur $b, c$ et $d$.
   2. Résoudre l'équation $P = 0$ dans le cas où $q = 0$ et $p≠0$.
3. Résolution du cas général.
   
   On va chercher les solutions sous la forme $Y = u + v$.
   1. Remplacer $Y$ par $u + v$ et développer.
   2. On impose que $uv = \frac{-p}{3}$. Justifier que c'est possible.
   3. Réécrire l'équation précédente avec cette nouvelle condition.
   4. Donner la condition qui lie $q, u^3$ et $v^3$.
4. Recherche des cubes

   1. On cherche $u^3$ et $v^3$ connaissant leur somme et leur produit.
      Donner une méthode pour trouver $u^3$ et $v^3$.
   2. Si $x_1$ est une solution, alors
     + si $u$ et $v$ sont égaux $x_1$ est
       une solution double et en déduire la dernière solution.
     + sinon déterminer $x_1$ et $x_2$.
```

Pour la résolution de l'équation du troisième degré, Cardan a démontrer
qu'une des racines se calculait avec la formule

  $$
    x = \sqrt[3]{\frac{-q}2 + \sqrt{\frac{q^2}{4} + \frac{p^3}{27}}} + \sqrt[3]{\frac{-q}2 - \sqrt{\frac{q^2}{4} + \frac{p^3}{27}}}
  $$

```{exercise}
:label: resolution_3eme_degre
1. Résoudre l'équation $x^3 - 2x - 1 = 0$
2. Résoudre l'équation $x^3 - 3x + 2 = 0$
```
```{solution} resolution_3eme_degre
:class: dropdown
1. $S = \{ 1, -\frac12 + \frac{\sqrt{5}}2, -\frac12 - \frac{\sqrt{5}}2\}$
2. $S = \{ -2, 1 \}$
```

Cependant, la recherche de la solution certaine peut conduire à des
écritures étranges. C'est la seule et unique fois où on utilisera
$\sqrt{-1}$.

```{exercise}
On considère l'équation $x^3 - 15x -4 =0$

1. Vérifier que 4 est une solution de l'équation.
2. Quel est le problème de la méthode ?
3. On va supposer que $\sqrt{-121}$ est une écriture légitime. Donner la
   propriété utilisée pour écrire $\sqrt{-121} = 11\sqrt{-1}$.
4. On va chercher un cube qui vaut $2 ± 11 \sqrt{-1}$ sous la forme $2 +
   a\sqrt{-1}$. Développer $(2 + a\sqrt{-1})^3$ en utilisant
   $\sqrt{-1}×\sqrt{-1} = -1$. En identifiant avec $2 + 11\sqrt{-1}$,
  déterminer $a$.
5. Retrouver $x = 4$ par le calcul.
```


