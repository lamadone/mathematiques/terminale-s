# Nombres complexes sous forme algébrique

```{list-table}
:header-rows: 1
* - Contenus
  - Capacités attendues
  - Démonstrations
* - - Ensemble **C** des nombres complexes.
    Partie réelle et imaginaire
    Opérations
    - Conjugaison, propriétés algébriques
    - Inverse d'un nombre complexe non nul
    - Formule du binôme de Newton dans **C**
  - - Effectuer des calculs algébriques avec des nombres complexes
    - Résoudre une équation linéaire $az = b$
    - Résoudre une équation simple faisant intervenir $z$ et $\overline{z}$
  - - Conjugué d'un produit, d'un inverse, d'une puissance entière
    - Formule du binôme
```

