# Cours sur les nombres complexes

## Définitions et premières propriétés

```{prf:definition}
On définit l'ensemble $\mathbf{C}$ des nombres complexes comme étant l'ensemble
des nombres écrits sous la forme $z = a +ib$, $a \in \mathbf{R}$, $b \in \mathbf{R}$.
```

```{margin}
Lorsque $a \in \mathbf{R}$ et $b \in \mathbf{R}$, on peut noter $(a, b) \in \mathbf{R}×\mathbf{R}$, ce qui s'abrège en $\mathbf{R}^2$.
````

```{prf:definition}
Pour un nombre $z = a + ib$, on note $\Re{z} = a$ sa partie réelle et $\Im{z} = b$ sa partie imaginaire.
```

```{prf:proposition} Règle de calcul
Soient $z = a + ib$ et $z' = a' + ib'$ deux nombres complexes.
+ $z + z' = a + a' + i(b + b')$
+ $z - z' = a - a' + i(b - b')$
+ $z ⋅ z' = aa' - bb' + i(ab' + a'b)$
```

```{prf:definition} Conjugué
Soit $z = a + ib\in \mathbf{C}$ un nombre complexe. On appelle conjugué de $z$ le nombre noté $\overline{z} = a - ib$.
```

````{margin}
```{note}
On dispose donc désormais de la factorisation $x^2 + y^2 = (x + iy)(x -
    iy)$.
```
````
```{prf:proposition}
Soit $z = x + i y$ un nombre complexe. $z\overline{z} \in \mathbf{R}_+$ avec
$z\overline{z} = 0 \iff z = 0$.
```

```{prf:definition}
Le nombre $\sqrt{z\overline{z}}$ s'appelle le module du nombre complexe $z$.
Il est noté $\|z\|$.
```


