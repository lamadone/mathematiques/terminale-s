# Organisation du cours

Cette page liste les modalités pratiques du cours de mathématiques expertes.

## Conseils pour l'année

Pour suivre le cours dans de bonnes conditions, il est demandé d'avoir un
classeur souple avec des feuilles petits carreaux et quelques pochettes
transparentes.

L'écriture des mathématiques et la structuration de la pensée passant par
l'écrit en action, le cours ne sera que rarement distribué sous forme
papier. Il sera parfois projeté et accessible sur ce même site web, mais la
prise en notes de ce cours est un élément essentiel de l'activité.

La trace des recherches, même entachée d'erreurs, est un élément essentiel
de l'activité mathématiques et a donc tout sa place dans vos notes et
résolutions d'exercices, aussi bien que le corrigé. À ce titre, on proscrira
en classe blanc correcteur et gomme, de façon à conserver les fausses pistes
aussi bien que les corrections.

La calculatrice et le matériel léger de géométrie (a minima règle et compas)
doivent permettre de confirmer rapidement une idée ou de schématiser une
résolution, ou encore d'automatiser un traitement. Ce sont donc des outils
du quotidien.

Pour résumer :
+ un classeur souple
  + pour noter le cours
  + pour chercher
  + pour noter les corrections
+ calculatrice, règle et compas
  + pour faire un schéma
  + pour calculer rapidement
  + pour automatiser

## Les compétences mathématiques

Les cours de mathématiques au lycée cherchent tous à développer six
compétences majeures :
+ Chercher
+ Modéliser
+ Représenter
+ Raisonner
+ Calculer
+ Communiquer

Ces compétences sont entre autre utilisés dans le cadre de la construction
de l'appréciation trimestrielle.
